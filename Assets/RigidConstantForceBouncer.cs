﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigidConstantForceBouncer : MonoBehaviour
{
	public Vector2 ConstantVelocity;
	public float ConstantRotation;
	private Rigidbody2D Rbody;
    // Start is called before the first frame update
    void Start()
    {
		Rbody = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
		Rbody.velocity = ConstantVelocity;
		Rbody.rotation = ConstantRotation;

	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		var hit = collision.GetContact(0); // the first contact is enough
		//hit.normal.y = 0; // keep normal in the horizontal plane
		//Debug.DrawRay(hit.point, hit.normal, Color.green, 1); // draw green normal
		//Debug.DrawRay(hit.point, -ConstantVelocity, Color.red, 1); // draw red "in" velocity
		ConstantVelocity = Vector3.Reflect(ConstantVelocity, hit.normal); // reflect the velocity
		//Debug.DrawRay(hit.point, ConstantVelocity, Color.blue, 1); // draw blue "out" velocity
	}
}
