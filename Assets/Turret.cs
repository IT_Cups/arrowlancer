﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : BumpyMob, ITargeting
{
	[Header("General")]
	public float MaxDistanceToTarget = 3;
	public float DelayBetweenShoots = 2;
	public float TurnSpeed = 1;

	[Header("Weapon")]
	public GameObject ProjectilePrefab;
	public float ProjectileMaxSpeed = 2;
	public float ProjectileSlowdown = 1f;
	public float AimSway = 2;

	private Vector2 targetDirection;


	public IGameObject CurrentTarget { get; private set; } = null;



	// Start is called before the first frame update
	protected override void Start()
	{
		base.Start();
		StartCoroutine(SearchForTarget());

	}

	public void Fire()
	{
		var arrow = Instantiate(ProjectilePrefab, transform.position, transform.rotation);
		arrow.layer = gameObject.layer;
		var arrowScript = arrow.GetComponent<ProjectileDistance>();
		arrowScript.CurrentSpeed = ProjectileMaxSpeed;
		arrowScript.CurrentDirection = transform.rotation * Vector2.up;
		arrowScript.LifeDistanceLeft = MaxDistanceToTarget;
		arrowScript.SlowdownPerSecond = ProjectileSlowdown;
		CurrentTarget = null;

	}

	private IEnumerator SearchForTarget()
	{
		yield return new WaitForSeconds(1);
		while (CurrentTarget == null)
		{

			GetTarget();
			yield return new WaitForSeconds(1);

		}
		StartCoroutine(AimAtTarget());

	}



	private IEnumerator AimAtTarget()
	{
		var inaccuracy = Random.Range(0, AimSway);
		while (CurrentTarget != null)
		{
			transform.RotateTowards2D(targetDirection, TurnSpeed * Time.deltaTime);

			Quaternion q = Quaternion.LookRotation(Vector3.forward, targetDirection);
			if (Quaternion.Angle(transform.rotation, q) < inaccuracy)
			{
				Fire();
				yield return new WaitForSeconds(DelayBetweenShoots);
				StartCoroutine(SearchForTarget());
				yield break;
			}
			yield return null;
		}
		StartCoroutine(SearchForTarget());


	}

	public IGameObject GetTarget()
	{
		if (CurrentTarget == null)
		{
			var t = this.FindTargetFactionBased(MaxDistanceToTarget);
			if (t.HasValue)
			{
				CurrentTarget = t.Value.Item1;
				targetDirection = t.Value.Item2;
			}
		}

		return CurrentTarget;
	}
}
