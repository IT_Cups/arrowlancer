﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using DG.Tweening;

public class EnergyOrb : MonoBehaviour, ITargeting
{
	
	public float Energy = 5;
	public IGameObject CurrentTarget => sp;

	private SpaceKnight sp;
	private float speed = 0;
	private IEnumerator animationCoroutine = null;

	

	// Start is called before the first frame update
	void Start()
	{
		GetTarget();
	}

	// Update is called once per frame
	void Update()
	{


	}

	public void AddEnergy()
	{
		sp.SetEnergy(sp.Energy + Energy);
		Destroy();
	}

	private IEnumerator CollectAnimation()
	{
		while (CurrentTarget != null)
		{
			transform.position = Vector3.MoveTowards(transform.position, CurrentTarget.transform.position, speed * Time.deltaTime);
			if (gameObject.transform.position == CurrentTarget.transform.position)
			{
				AddEnergy();
				yield break;
			}
			yield return null;
		}
	}


	public void PlayAnimation()
	{
		if (animationCoroutine != null)
			return;
		
		DOTween.To(() => speed, x => speed = x, 8, 1.2f).SetEase(Ease.InCubic);
		animationCoroutine = CollectAnimation();
		StartCoroutine(animationCoroutine);

	}
	public void Destroy()
	{
		Destroy(gameObject);
	}
	void OnDrawGizmos()
	{

	}

	public IGameObject GetTarget()
	{
		if(sp == null)
			sp = GameController.Players[GameController.PlayerNumber.Player1].Avatar as SpaceKnight;

		return CurrentTarget;
	}
}

