﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DualSliderControl : MonoBehaviour, IUIElement
{
	// Start is called before the first frame update
	[Range(0,1)]
	public float Value = 1;
	public GameObject SliderOne;
	public GameObject SliderTwo;
	private Slider slider1;
	private Slider slider2;

	public string elementID;
	public string id => elementID;

	public bool IsHidden => !gameObject.activeSelf;

	void Awake()
    {
		UIController.AddElement(id, this);
	}

    // Update is called once per frame
    void Update()
    {
        
    }

	public void SetSliderValues(float V)
	{
		if (SliderTwo == null || SliderOne == null)
		{
			Debug.LogError("One or more sliders are not set");
			return;
		}
		slider1.value = slider1.maxValue * V;
		slider2.value = slider2.maxValue * V;
	}
	private void OnValidate()
	{
		slider1 = SliderOne.GetComponent<Slider>();
		slider2 = SliderTwo.GetComponent<Slider>();
		SetSliderValues(Value);
	}

	public void Hide()
	{
		gameObject.SetActive(false);
	}

	public void Show()
	{
		gameObject.SetActive(true);
	}
}
