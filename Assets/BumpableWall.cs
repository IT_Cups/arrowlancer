﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BumpableWall : MonoBehaviour, IBumpable
{
	public float BumpForceMultiplier = 1;
	public void Bump(IBumpable bumpedObject, Vector3 force, Vector3 point)
	{
		bumpedObject.gameObject.GetComponent<Rigidbody2D>().AddForceAtPosition(force, point, ForceMode2D.Impulse);
	}

	protected virtual void OnCollisionEnter2D(Collision2D collision)
	{
		var hit = collision.GetContact(0);

		IBumpable bumpable = collision.gameObject.GetComponent<IBumpable>();

		if (bumpable != null && CanBump(bumpable))
		{ 
			Rigidbody2D bumpedRB = bumpable.gameObject.GetComponent<Rigidbody2D>();
			Bump(bumpable, -bumpable.transform.WorldPositionToRelative(hit.point).normalized * Mathf.Max(BumpForceMultiplier, bumpedRB.velocity.magnitude), hit.point);
		}
	}

	public bool CanBump(IBumpable bumpedObject)
	{
		return true;
	}

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
