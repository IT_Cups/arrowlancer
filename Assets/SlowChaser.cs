﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowChaser : BumpyMob, ITargeting
{
	public float MaxDistanceToTarget = 10;
	public float MaxSpeed = 1;
	public float TurnSpeed = 1;

	public IGameObject CurrentTarget { get; set; }
	private Vector2 targetDirection;


	// Start is called before the first frame update
	protected override void Start()
	{
		base.Start();
		StartCoroutine(FindTarget());

	}
	protected override void Awake()
	{
		base.Awake();
		Collider = GetComponent<Collider2D>();
	}


	protected override void OnCollisionEnter2D(Collision2D collision)
	{
		base.OnCollisionEnter2D(collision);

	}

	public override void Bump(IBumpable bumpedObject, Vector3 force, Vector3 point)
	{
		Debug.Log("Bumped");
		Stop();
		base.Bump(bumpedObject, force, point);

	}

	public override void Stop()
	{
		CurrentTarget = null;
		base.Stop();
		StopAllCoroutines();

		StartCoroutine(FindTarget());

	}
		
	
	void OnDrawGizmosSelected()
	{
		if (CurrentTarget != null)
		{
			Gizmos.color = Color.yellow;
			Gizmos.DrawSphere(CurrentTarget.transform.position, 0.5f);
		}
	}

	public IGameObject GetTarget()
	{
		if (CurrentTarget == null)
		{
			var t = this.FindTargetFactionBased(MaxDistanceToTarget);
			if(t.HasValue)
			{
				CurrentTarget = t.Value.Item1;
				targetDirection = t.Value.Item2;
			}
		}
		return CurrentTarget;
	}
	

	private IEnumerator FindTarget()
	{

		while (CurrentTarget == null)
		{
			yield return new WaitForSeconds(1);
			GetTarget();

		}

		StartCoroutine(TrackTarget());
		StartCoroutine(ChaseTarget());

	}
	private IEnumerator TrackTarget()
	{
		yield return new WaitForSeconds(0.5f);
		while (CurrentTarget != null)
		{
			var path = TeleporterExtention.FindShortestStraightPathTo(transform.position, CurrentTarget.transform.position, MaxDistanceToTarget, Collider, (CurrentTarget as ICollider2D).Collider);
			if (path.HasValue)
			{
				targetDirection = (Vector2)path;
			}
			yield return new WaitForSeconds(0.5f);
		}
		Stop();

	}
	private IEnumerator ChaseTarget()
	{
		while (CurrentTarget != null)
		{

			transform.RotateTowards2D(targetDirection, TurnSpeed * Time.deltaTime);

			Move(transform.up, MaxSpeed);

			yield return null;
		}
		Stop();


	}

}
