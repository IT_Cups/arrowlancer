using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine.InputSystem.PointerManager;
using UnityEngine;
using UnityEngine.UI;

namespace UnityEngine.InputSystem.Gestures
{
    /// <summary>
    /// Manager that interprets takes pointer input from <see cref="PointerInputManager"/> and detects
    /// directional swipes and detects taps.
    /// </summary>
    public class GestureManager : MonoBehaviour
    {
        [SerializeField]
        private PointerInputManager inputManager = null;

        // Maximum duration of a press before it can no longer be considered a tap.
        [SerializeField]
        private float maxTapDuration = 0.2f;

        // Maximum distance in screen units that a tap can drift from its original position before
        // it is no longer considered a tap.
        [SerializeField]
        private float maxTapDrift = 5.0f;

		// Maximum time interval between taps that will be counted towards multitap.
		[SerializeField]
		private float maxTapInterval = 0.5f;

		// Maximum duration of a swipe before it is no longer considered to be a valid swipe.
		[SerializeField]
        private float maxSwipeDuration = 0.5f;

        // Minimum distance in screen units that a swipe must move before it is considered a swipe.
        // Note that if this is smaller or equal to maxTapDrift, then it is possible for a user action to be
        // returned as both a swipe and a tap.
        [SerializeField]
        private float minSwipeDistance = 10.0f;

        // How much a swipe should consistently be in the same direction before it is considered a swipe.
        [SerializeField]
        private float swipeDirectionSamenessThreshold = 0.6f;

		private List<ActiveGesture> tapHistory = new List<ActiveGesture>();




		[Header("Debug")]
        public Text label;

        // Mapping of input IDs to their active gesture tracking objects.
        private readonly Dictionary<int, ActiveGesture> activeGestures = new Dictionary<int, ActiveGesture>();

        /// <summary>
        /// Event fired when the user presses on the screen.
        /// </summary>
        public event Action<SwipeInput> Pressed = null;

        /// <summary>
        /// Event fired for every motion (possibly multiple times a frame) of a potential swipe gesture.
        /// </summary>
        public event Action<SwipeInput> PotentiallySwiped;

        /// <summary>
        /// Event fired when a user performs a swipe gesture.
        /// </summary>
        public event Action<SwipeInput> Swiped;

        /// <summary>
        /// Event fired when a user performs a tap gesture, on releasing.
        /// </summary>
        public event Action<TapInput> Tapped;

		/// <summary>
		/// Event fired when a user performs a multi tap gesture, on releasing.
		/// </summary>
		public event Action<MultiTapInput> MultiTapped;

		protected virtual void Awake()
        {
            inputManager.Pressed += OnPressed;
            inputManager.Dragged += OnDragged;
            inputManager.Released += OnReleased;
        }

        /// <summary>
        /// Checks whether a given active gesture will be a valid swipe.
        /// </summary>
        private bool IsValidSwipe(ref ActiveGesture gesture)
        {
            return gesture.TravelDistance >= minSwipeDistance &&
                (gesture.StartTime - gesture.EndTime) <= maxSwipeDuration &&
                gesture.SwipeDirectionSameness >= swipeDirectionSamenessThreshold;
        }

        /// <summary>
        /// Checks whether a given active gesture will be a valid tap.
        /// </summary>
        private bool IsValidTap(ref ActiveGesture gesture)
        {
            return gesture.TravelDistance <= maxTapDrift &&
                (gesture.StartTime - gesture.EndTime) <= maxTapDuration;
        }

        private void OnPressed(PointerInput input, double time)
        {
            Debug.Assert(!activeGestures.ContainsKey(input.InputId));

            var newGesture = new ActiveGesture(input.InputId, input.Position, time);
            activeGestures.Add(input.InputId, newGesture);

            DebugInfo(newGesture);

            Pressed?.Invoke(new SwipeInput(newGesture));
        }

        private void OnDragged(PointerInput input, double time)
        {
            if (!activeGestures.TryGetValue(input.InputId, out var existingGesture))
            {
                // Probably caught by UI, or the input was otherwise lost
                return;
            }

            existingGesture.SubmitPoint(input.Position, time);

            if (IsValidSwipe(ref existingGesture))
            {
                PotentiallySwiped?.Invoke(new SwipeInput(existingGesture));
            }

            DebugInfo(existingGesture);
        }

        private void OnReleased(PointerInput input, double time)
        {
            if (!activeGestures.TryGetValue(input.InputId, out var existingGesture))
            {
                // Probably caught by UI, or the input was otherwise lost
                return;
            }
			activeGestures.Remove(input.InputId);
            existingGesture.SubmitPoint(input.Position, time);


			if (IsValidSwipe(ref existingGesture))
			{
				Swiped?.Invoke(new SwipeInput(existingGesture));
			}

			else if (IsValidTap(ref existingGesture))
			{
				Tapped?.Invoke(new TapInput(existingGesture));


				foreach(var entry in tapHistory)
				{
					if (Mathf.Abs(existingGesture.EndPosition.magnitude - entry.EndPosition.magnitude) >= maxTapDrift)
					{
						tapHistory.Clear();
						break;
					}
				}
				// removing previous taps that are old
				while (tapHistory.Count > 0 && time >= tapHistory[0].EndTime + maxTapInterval)
					tapHistory.RemoveAt(0);

				tapHistory.Add(existingGesture);

				if (tapHistory.Count > 1)
					MultiTapped?.Invoke(new MultiTapInput(existingGesture, tapHistory.Count));
			}

			DebugInfo(existingGesture);
        }

        private void DebugInfo(ActiveGesture gesture)
        {
            if (label == null) return;

            var builder = new StringBuilder();

            builder.AppendFormat("ID: {0}", gesture.InputId);
            builder.AppendLine();
            builder.AppendFormat("Start Position: {0}", gesture.StartPosition);
            builder.AppendLine();
            builder.AppendFormat("Position: {0}", gesture.EndPosition);
            builder.AppendLine();
            builder.AppendFormat("Duration: {0}", gesture.EndTime - gesture.StartTime);
            builder.AppendLine();
            builder.AppendFormat("Sameness: {0}", gesture.SwipeDirectionSameness);
            builder.AppendLine();
            builder.AppendFormat("Travel distance: {0}", gesture.TravelDistance);
            builder.AppendLine();
            builder.AppendFormat("Samples: {0}", gesture.Samples);
            builder.AppendLine();
            builder.AppendFormat("Realtime since startup: {0}", Time.realtimeSinceStartup);
            builder.AppendLine();
            builder.AppendFormat("Starting Timestamp: {0}", gesture.StartTime);
            builder.AppendLine();
            builder.AppendFormat("Ending Timestamp: {0}", gesture.EndTime);
            builder.AppendLine();

            label.text = builder.ToString();

            var worldStart = Camera.main.ScreenToWorldPoint(gesture.StartPosition);
            var worldEnd = Camera.main.ScreenToWorldPoint(gesture.EndPosition);

            worldStart.z += 5;
            worldEnd.z += 5;
        }

    }
}
