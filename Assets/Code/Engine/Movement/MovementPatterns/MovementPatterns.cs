﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovementPattern : MonoBehaviour
{
	public int lastMovement = 0;
	public bool IsMoving { get=> Time.frameCount - lastMovement > 1 ? true : false; }
	public abstract Vector3 Move(Vector3 vector, float maxSpeed);
	public void Reset()
	{

	}
}
public class Linear : MovementPattern
{
	public override Vector3 Move(Vector3 vector, float maxSpeed)
	{
		if (vector.magnitude == 0)
			return Vector3.zero;
		Vector3 lastPos = gameObject.transform.position;

		/*
		switch (mode)
		{
			case ComponentBased.RigidBody2D:
				{
					Rigidbody2D ridigdBody = gameObject.GetComponent<Rigidbody2D>();
					if (!ridigdBody)
					{
						Debug.LogError("WRONG USAGE IMovementPattern");
						return;
					}
					var distance = gameObject.GetDistance(destination);
					float travelDistance = speed * Time.deltaTime;

					// TODO: fix this (travelDistance > distance ? distance/Time.deltaTime : speed)
					if (distance > approachDistance)
						ridigdBody.velocity = gameObject.GetDirection(destination) * (travelDistance > distance ? distance / Time.deltaTime : speed);
					else
						ridigdBody.velocity = Vector2.zero;
					break;
				}
			case ComponentBased.None:
				{*/
		gameObject.transform.position = gameObject.transform.position + vector.normalized * Mathf.Min(maxSpeed * Time.smoothDeltaTime, vector.magnitude);
		lastMovement = Time.frameCount;
		return gameObject.transform.position - lastPos;
	}
}
	
