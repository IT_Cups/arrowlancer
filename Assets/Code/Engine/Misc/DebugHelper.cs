﻿using UnityEngine;
using System.Collections;

public static class DebugHelper
{
	public static void DrawGizmoArrow(Vector3 pos, Vector3 direction, Color color, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
	{
		Gizmos.color = color;
		Gizmos.DrawRay(pos, direction);

		Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0) * new Vector3(0, 0, 1);
		Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0) * new Vector3(0, 0, 1);
		Gizmos.DrawRay(pos + direction, right * arrowHeadLength);
		Gizmos.DrawRay(pos + direction, left * arrowHeadLength);
	}
	/*
	public static void DrawDebugArrow(Vector3 pos, Vector3 direction, Color color, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
	{
		Debug.DrawRay(pos, direction, color);

		Vector3 right = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 + arrowHeadAngle, 0) * new Vector3(0, 0, 1);
		Vector3 left = Quaternion.LookRotation(direction) * Quaternion.Euler(0, 180 - arrowHeadAngle, 0) * new Vector3(0, 0, 1);
		Debug.DrawRay(pos + direction, right * arrowHeadLength, color);
		Debug.DrawRay(pos + direction, left * arrowHeadLength, color);
	}*/
	public static void DrawDebugArrow(Vector3 pos, Vector3 direction, Color color, float arrowHeadLength = 0, float arrowHeadAngle = 20.0f , float duration = 0)
	{
		if (direction.magnitude < 0.01)
			return;
		Debug.DrawRay(pos, direction, color, duration);

		Vector3 right = Quaternion.AngleAxis(180 - arrowHeadAngle, Vector3.Cross(direction, Vector3.up)) * direction;
		right = Quaternion.AngleAxis(45, direction) * right;
		Vector3 down = Quaternion.AngleAxis(90, direction) * right;
		Vector3 left = Quaternion.AngleAxis(90 ,direction) * down;
		Vector3 up = Quaternion.AngleAxis(90, direction) * left;
		
		if (arrowHeadLength == 0)
			arrowHeadLength = direction.magnitude / 5;
		Debug.DrawRay(pos + direction, right.normalized * arrowHeadLength, color, duration);
		Debug.DrawRay(pos + direction, left.normalized * arrowHeadLength, color, duration);
		Debug.DrawRay(pos + direction, up.normalized * arrowHeadLength, color, duration);
		Debug.DrawRay(pos + direction, down.normalized * arrowHeadLength, color, duration);
	}

	public static void DrawGizmoCross(Color color, Vector3 position, float radius = 0.5f)
	{
		Gizmos.color = color;
		Gizmos.DrawLine(new Vector3(position.x - radius, position.y, position.z - radius), new Vector3(position.x + radius, position.y, position.z + radius));
		Gizmos.DrawLine(new Vector3(position.x, position.y - radius, position.z - radius), new Vector3(position.x, position.y + radius, position.z + radius));
		Gizmos.DrawLine(new Vector3(position.x - radius, position.y, position.z), new Vector3(position.x + radius, position.y, position.z));
	}

	public static void DrawDebugCross(Color color, Vector3 position, float radius = 0.5f, float duration = 0)
	{
		Debug.DrawLine(new Vector3(position.x - radius, position.y, position.z - radius), new Vector3(position.x + radius, position.y, position.z + radius), color, duration);
		Debug.DrawLine(new Vector3(position.x, position.y - radius, position.z - radius), new Vector3(position.x, position.y + radius, position.z + radius), color, duration);
		Debug.DrawLine(new Vector3(position.x - radius, position.y, position.z), new Vector3(position.x + radius, position.y, position.z), color, duration);
	}

}