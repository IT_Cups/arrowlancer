﻿using UnityEngine;
using System.Collections;


[System.Serializable]
public class Faction
{
	public string Name;

	public StandingEntry[] Standings;
	[System.Serializable]
	public struct StandingEntry
	{
		public string Name;
		public int Standing;
	}
	public int GetStanding(string name)
	{
		StandingEntry? found = null;
		foreach (var entry in Standings)
		{
			if (entry.Name == name)
			{
				found = entry;
				break;
			}
		}
		if (found != null)
			return found.Value.Standing;
		else
			return 0;
	}
	public void Validate()
	{
		Name = Name.ToLower();
		for(int i = 0; i< Standings.Length; i++)
		{
			Standings[i].Name = Standings[i].Name.ToLower();


		}
	}

}

