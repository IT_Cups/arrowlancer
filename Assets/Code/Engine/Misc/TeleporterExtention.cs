﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TeleporterExtention
{
	// NOTES:
	// Teleporters sometimes uses negative scale to mirror pos of teleportating object
	public static Orientation GetGhostOrientation(Transform obj, Transform enterTeleporter, Transform exitTeleporter, Vector3 enterPoint, bool keepRotation = false)
	{
		Orientation outOfTeleporter;
		outOfTeleporter.position = obj.position;
		outOfTeleporter.rotation = obj.rotation;
		outOfTeleporter.localScale = obj.localScale;
		Vector3 objPosRelativeToEnterPoint = enterTeleporter.RelativePositionToWorld(enterPoint) - obj.position;
		// rotatating by difference in angle between teles
		objPosRelativeToEnterPoint = exitTeleporter.rotation * (Quaternion.Inverse(enterTeleporter.rotation) * objPosRelativeToEnterPoint);
		// then apply relative pos to exit point and get ghost position
		outOfTeleporter.position = exitTeleporter.RelativePositionToWorld(enterPoint) + objPosRelativeToEnterPoint.Round();

		if (keepRotation)
			outOfTeleporter.rotation = obj.rotation;
		return outOfTeleporter;
	}

	/// <summary>
	/// Cast a ray against colliders in the scene, but respect teleporter objects and runs ray throught them.
	/// </summary>
	/// /// <remarks>
	/// Note that return data RaycastHit2D represents only way from last teleporter till end point.
	/// </remarks>
	/// <param name="startingPos"></param>
	/// <param name="dir"></param>
	/// <param name="distance"></param>
	/// <param name="ignoreColliders"></param>
	/// <param name="layers">Layers to raycast to. Note that layers are bitflag</param>
	/// <returns></returns>
	public static RaycastHit2D Raycast2DTeleporter(Vector3 startingPos, Vector3 dir, float distance, List<Collider2D> ignoreColliders = null, int layers = Physics2D.DefaultRaycastLayers)
	{
		ignoreColliders = ignoreColliders ?? new List<Collider2D>(); 
		RaycastHit2D hit = Physics2DExt.Raycast2DExtended(startingPos, dir, distance , ignoreColliders, layers);
		
		if(hit.collider != null)
			{
			
			var tele = hit.collider.GetComponent<ITeleporter2D>();
			if(tele != null)
				{
				var newStartPos = tele.ConnectedTeleporter.transform.RelativePositionToWorld(tele.transform.WorldPositionToRelative(hit.point));
				ignoreColliders.Add(tele.Collider);
				ignoreColliders.Add((tele.ConnectedTeleporter as ITeleporter2D).Collider);
				hit = Raycast2DTeleporter(newStartPos, dir, distance - hit.distance, ignoreColliders);
				}
			}
		return hit;
	}

	public static T GetComponentWithTeleporter<T>(this GameObject thisComp)
	{
		T returnValue = thisComp.GetComponent<T>();
		if(returnValue == null)
			{
			var tele = thisComp.GetComponent<ITeleporter>();
			if (tele != null)
				{
				returnValue = tele.gameObject.GetComponentWithTeleporter<T>();
				}	
		}
		return returnValue;
	}
	/// <summary>
	/// Will parse through all teleporters and try to find straight path to destination collider
	/// </summary>
	/// <returns>Vector3 representing straight path or NULL if not found</returns>
	public static Vector3? FindStraightTeleporterPathTo(Vector3 origin, Vector3 destination, float maxDistance, Collider2D originCollider = null, Collider2D destinationCollider = null, bool returnFirst = false)
	{
		Vector3? returnValue = null;
		foreach(ITeleporter tele in GameController.Teleporters)
		{
			// rough distance estimation check
			if (Vector3.Distance(origin, tele.transform.position) > maxDistance)
				continue;
			if (Vector3.Distance(tele.ConnectedTeleporter.transform.position, destination) > maxDistance)
				continue;

			// destination position as if it was when teleporters taken in consideration
			Vector3 destPosThroghTele = tele.transform.RelativePositionToWorld(Vector3.Reflect(tele.ConnectedTeleporter.transform.WorldPositionToRelative(destination),tele.ConnectedTeleporter.transform.up * -1));
			if (Vector3.Distance(origin, destPosThroghTele) > maxDistance)
				continue;
			Vector3 straightPath = destPosThroghTele - origin;
			if(destinationCollider !=null)
			{
				RaycastHit2D hit = Raycast2DTeleporter(origin, straightPath, maxDistance, ignoreColliders: new List<Collider2D>() { originCollider });
				if (hit.collider == destinationCollider)
				{
					if (returnFirst)
						return straightPath;
					else if (!returnValue.HasValue || returnValue.Value.sqrMagnitude > straightPath.sqrMagnitude)
						returnValue = straightPath;
				}
			}
			else
			{
				RaycastHit2D hit = Raycast2DTeleporter(origin, straightPath, maxDistance, layers: LayerMask.GetMask("Teleporter"));
				if (hit.collider == null)
				{
					if (returnFirst)
						return straightPath;
					else if (!returnValue.HasValue || returnValue.Value.sqrMagnitude > straightPath.sqrMagnitude)
						returnValue = straightPath;
				}
			}
		}
		return returnValue;
	}

	public static Vector3? FindShortestStraightPathTo(Vector3 origin, Vector3 destination, float maxDistance, Collider2D originCollider =null, Collider2D destinationCollider=null)
	{
		Vector3? telePath = FindStraightTeleporterPathTo(origin, destination, maxDistance, originCollider, destinationCollider);
		Vector3? straightPath = destination - origin;
		
		
		if (straightPath.Value.magnitude > maxDistance)
			straightPath = null;

			if(straightPath.HasValue)
			{
				DebugHelper.DrawDebugArrow(origin, (Vector2)straightPath, Color.green, duration: 0.5f);
				if (Physics2DExt.CheckPathCollisions(origin, straightPath.Value, maxDistance, new List<Collider2D>() { originCollider }, destinationCollider))
					straightPath = null;
			}
			if(telePath.HasValue)
			{
				DebugHelper.DrawDebugArrow(origin, (Vector2)telePath, Color.blue, duration: 0.5f);
				if (TeleporterExtention.CheckPathCollisions(origin, telePath.Value, maxDistance, new List<Collider2D>() { originCollider }, destinationCollider))
					telePath = null;

		}
				
			if (straightPath.HasValue && telePath.HasValue)
				return straightPath.Value.sqrMagnitude <= telePath.Value.sqrMagnitude ? straightPath : telePath;
			else
				return straightPath.HasValue ? straightPath : telePath;

		
	}


	public static bool CheckPathCollisions(Vector3 startingPos, Vector3 dir, float distance, List<Collider2D> ignoreColliders, Collider2D destinationCollider = null, int layers = Physics2D.DefaultRaycastLayers)
	{
		var hit = TeleporterExtention.Raycast2DTeleporter(startingPos, dir, distance, ignoreColliders, layers);
		if (hit.collider != null)
			if (destinationCollider && hit.collider == destinationCollider)
				return false;
			else
				return true;

		return false;
	}
} 
