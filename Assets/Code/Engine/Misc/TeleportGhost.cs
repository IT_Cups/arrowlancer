﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// TeleportGhost needs fixedupdate to check collision to finilaze teleport 
public class TeleportGhost : MonoBehaviour
{
	public bool KeepRotation;
	public ITeleportable OriginalObject { get; private set; }
	// relative point from teleporter where originObject have entered
	public Vector3 EnterPoint { get; private set; }
	public ITeleporter EnterTeleporter { get; private set; }
	public ITeleporter ExitTeleporter { get; private set; }
	// list of scripts that mimics certain component on originObject
	public List<ICopyCat> CopyCats = new List<ICopyCat>();



	public void Init(ITeleportable orgObject, Vector3 enter, ITeleporter teleStart, ITeleporter teleEnd, bool keepObjectRotation = true)
	{
		OriginalObject = orgObject;
		OriginalObject.TeleportationGhost = this;
		EnterPoint = enter;
		EnterTeleporter = teleStart;
		ExitTeleporter = teleEnd;
		KeepRotation = keepObjectRotation;

		CreateCopyCats(gameObject);

		foreach (Transform child in transform)
		{
			CreateCopyCats(child.gameObject);
		}

	}

	public void CreateCopyCats(GameObject GO)
	{
		Component[] comps = GO.GetComponents<Component>();
		bool hasRigid = GO.GetComponent<Rigidbody2D>() ? true : false;
		foreach (Component C in comps)
		{
			switch (C)
			{
				case Transform T:
					if(!hasRigid)
						CopyCats.Add(T.CreateCopyCat(OriginalObject.gameObject, EnterPoint, EnterTeleporter.transform, ExitTeleporter.transform, KeepRotation));
					break;
				case Rigidbody2D T:
					CopyCats.Add(T.CreateCopyCat(OriginalObject.gameObject, EnterPoint, EnterTeleporter.transform, ExitTeleporter.transform, KeepRotation));
					break;
				default:
					//Debug.Log("No Avaliable copycat script for type \"" + C.GetType() + "\"");
					break;
			}
		}
	}

	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		if (OriginalObject == null)
			return;

	}

	public void FinalizeTeleport()
	{

		OriginalObject.transform.position = transform.position;
		if (KeepRotation)
			OriginalObject.transform.rotation = transform.rotation;

		EnterTeleporter.TeleportingObjects.Remove(OriginalObject.gameObject);
		ExitTeleporter.TeleportingGhosts.Remove(gameObject);

		OriginalObject.TeleportationGhost = null;
		Destroy(gameObject);

	}
	public void AbortTeleport()
	{
		EnterTeleporter.TeleportingObjects.Remove(OriginalObject.gameObject);
		ExitTeleporter.TeleportingGhosts.Remove(gameObject);
		OriginalObject.TeleportationGhost = null;
		Destroy(gameObject);
	}

}
