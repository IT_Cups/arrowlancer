﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryController : MonoBehaviour, IVictoryController
{
	public List<ICondition> VictoryConditions { get; set; } = new List<ICondition>();
	public void VictoryActions()
	{
		IUIElement victoryUI = UIController.Elements["victory"];
		victoryUI?.Show();
	}

	public void CheckVictoryConditions()
	{
		bool victory = true;
		foreach (var condition in VictoryConditions)
		{
			if (!condition.IsMet())
			{
				victory = false;
				break;
			}
		}
		if (victory)
			VictoryActions();
	}

	// Start is called before the first frame update
	void Start()
    {
		ICondition[] conds = FindObjectsOfType<DummyTarget>();
		VictoryConditions = conds.ToList();
	}
	// Update is called once per frame
	void Update()
    {
        
    }
}
