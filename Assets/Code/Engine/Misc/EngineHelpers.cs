﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace UnityEngine
{
	public enum Axis
	{
		None,
		X = 1 << 0,
		Y = 1 << 1,
		Z = 1 << 2
	}

	public enum Direction
	{
		None,
		Left = -1,
		Right = 1,
		Top = 2,
		Bottom = -2,
		Forward = 3,
		Backward = -3
	}

	public struct Orientation
	{
		public Vector3 position;
		public Quaternion rotation;
		public Vector3 localScale;

	}

	public class PreviewSpriteAttribute : PropertyAttribute
	{
		public PreviewSpriteAttribute() { }
	}
	public class PreviewTextureAttribute : PropertyAttribute
	{
		public PreviewTextureAttribute() { }
	}
	public static class EngineHelper
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="angle">Angle range is (0-360)</param>
		/// <returns></returns>
		public static Vector3 GetVectorFromAngle(float angle)
		{
			float angleRad = angle * (Mathf.PI / 180f);
			return new Vector3(Mathf.Cos(angleRad), Mathf.Sin(angleRad));
		}
		public static float GetAnglerFromVector(Vector3 dir)
		{
			dir = dir.normalized;
			float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
			if (n < 0) n += 360;
			return n;
		}
	}
}

