﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem.Gestures;
using UnityEngine.InputSystem.PointerManager;
using UnityEngine;
using System.Linq;

public static class GameController
{
	public enum PlayerNumber
	{
		Player1 = 1,
		Player2,
		Player3,
		Player4
	}

	public struct PlayerData
	{
		public PlayerNumber Number;
		public IPlayer Avatar;
		public GestureManager GestureManager;
		public PointerInputManager PointerManager;
	}

	public static Dictionary<PlayerNumber, PlayerData> Players { get; private set; } =  new Dictionary<PlayerNumber, PlayerData>();
	public static List<IMob> Mobs = new List<IMob>();
	public static List<ITeleporter> Teleporters = new List<ITeleporter>();

	public static void RegisterPlayer(PlayerNumber number, IPlayer avatar = null, PointerInputManager pm = null, GestureManager gm = null)
	{
		if (Players.ContainsKey(number))
			Debug.LogError("GameController already contains player with number " + number);
		else
		{
			PlayerData newChallenger = new PlayerData();
			newChallenger.Number = number;
			if(avatar != null)
				newChallenger.Avatar = avatar;
			if (pm != null)
				newChallenger.PointerManager = pm;
			if (gm != null)
				newChallenger.GestureManager = gm;
			Players.Add(number, newChallenger);
		}
	}
	public static void UnregisterPlayer(PlayerNumber number)
	{
		Players.Remove(number);
	}

	public static void AssignAvatar(PlayerNumber number, IPlayer avatar)
	{
		PlayerData player = Players[number];
		player.Avatar = avatar;
		Players[number] = player;

	}
	public static void AssignPointerManager(PlayerNumber number, PointerInputManager pm)
	{
		PlayerData player = Players[number];
		player.PointerManager = pm;
		Players[number] = player;
	}
	public static void AssignGestureManager(PlayerNumber number, GestureManager gm)
	{
		PlayerData player = Players[number];
		player.GestureManager = gm;
		Players[number] = player;
	}
}

