﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopycatTeleporterRigidbody2D : MonoBehaviour, ICopyCat
{
	public GameObject OriginalObject { get; set; }
	public bool KeepRotation = true;
	public Vector3 EnterPoint;
	public Transform EnterTeleporter;
	public Transform ExitTeleporter;
	private Rigidbody2D originRB;
	private Rigidbody2D RB;


	// Start is called before the first frame update
	void Start()
	{
		RB = GetComponent<Rigidbody2D>();
		originRB = OriginalObject.GetComponent<Rigidbody2D>();
	}

	private void FixedUpdate()
	{
		RB.velocity = originRB.velocity;
		RB.mass = originRB.mass;
		RB.rotation = originRB.rotation;
	}

	// Update is called once per frame
	void Update()
	{
		if (OriginalObject == null)
		{
			return;
		}

		var newOrientation = TeleporterExtention.GetGhostOrientation(OriginalObject.transform, EnterTeleporter, ExitTeleporter, EnterPoint);
		transform.position = newOrientation.position;
		transform.rotation = newOrientation.rotation;
	}

	void OnCollisionStay2D(Collision2D collision)
	{
		Vector2 impulse = collision.Impulse();
		if(impulse != Vector2.zero)
			originRB.AddForce(collision.Impulse(), ForceMode2D.Impulse);
		else
			{
			var newTransform = TeleporterExtention.GetGhostOrientation(OriginalObject.transform, EnterTeleporter, ExitTeleporter, EnterPoint);
			if(newTransform.position != transform.position)
				{
				var shouldBePos = TeleporterExtention.GetGhostOrientation(transform, ExitTeleporter, EnterTeleporter, ExitTeleporter.rotation * EnterPoint);
				OriginalObject.transform.position = shouldBePos.position;
				OriginalObject.transform.rotation = shouldBePos.rotation;
			}
			}
			
	}
}
