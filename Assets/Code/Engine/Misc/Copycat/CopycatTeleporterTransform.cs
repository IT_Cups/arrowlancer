﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopycatTeleporterTransform : MonoBehaviour, ICopyCat
{
	public GameObject OriginalObject { get; set; }
	public bool KeepRotation = true;
	public Vector3 EnterPoint;
	public Transform EnterTeleporter;
	public Transform ExitTeleporter;

	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		if (OriginalObject == null)
		{
			return;
		}

		Vector3 ghostPos = EnterTeleporter.RelativePositionToWorld(EnterPoint) - OriginalObject.transform.position;
		// and rotate it aswell
		ghostPos = ExitTeleporter.transform.rotation * (Quaternion.Inverse(EnterTeleporter.transform.rotation) * ghostPos);
		// then apply relative pos to exit point and get ghost position
		transform.position = ExitTeleporter.RelativePositionToWorld(EnterPoint).Round() + ghostPos.Round();
		if (KeepRotation)
			this.transform.rotation = OriginalObject.transform.rotation;
	}
}