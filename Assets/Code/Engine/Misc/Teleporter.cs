﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

public class Teleporter : MonoBehaviour, ITeleporter2D
{
	public bool Is2D = false;
	[Range(1, 360)]
	public float EnterArc = 180;
	public Vector3 FacingDirection { get => transform.rotation.ToDirection2D(); set { transform.rotation = Quaternion.Euler(value); } }

	public GameObject OtherTeleporter;
	public ITeleporter ConnectedTeleporter { get; set; }
	public Dictionary<GameObject, ITeleportable> TeleportingObjects { get; set; }
	public Dictionary<GameObject, TeleportGhost> TeleportingGhosts { get; set; }

	public Collider2D Collider { get; private set;}

	public int TeleGHosts = 0;
	public int TeleObjs = 0;

	protected void Awake()
	{
		TeleportingObjects = new Dictionary<GameObject, ITeleportable>();
		TeleportingGhosts = new Dictionary<GameObject, TeleportGhost>();

		//spriteRenderer.sprite = null;

		if (ConnectedTeleporter == null)
			Debug.LogError("Teleporter (" + gameObject.name + ") doesn't have connected teleporter.");
		Collider = GetComponent<Collider2D>();
		GameController.Teleporters.Add(this);
	}

	protected void Update()
	{
		TeleGHosts = TeleportingGhosts.Count;
		TeleObjs = TeleportingObjects.Count;
	}

	// TODO: PROPER CONNECTION, probably user Reset() to connect
	/*protected override void Awake()
    {
		base.Awake();
		if(!teleporterScript.connectedTeleporter)
		{
			List<TeleporterDive> possibleConnections = new List<TeleporterDive>();
			List<TileBase> axisTiles = new List<TileBase>();
			axisTiles.AddRange(ParentGrid.GetTilesByRow(ParentGrid.GetTilePosition(this).y));
			axisTiles.AddRange(ParentGrid.GetTilesByColumn(ParentGrid.GetTilePosition(this).x));
			List<TileBase> teleporterTiles = new List<TileBase>();
			foreach (var T in axisTiles)
				if (T is TileTeleporter)
					teleporterTiles.Add(T);
			// we will find furtheest teleporter to connect to
			TileTeleporter bestMatch = null;
			foreach (TileTeleporter T in teleporterTiles)
			{
				if (!bestMatch)
				{
					bestMatch = T;
					continue;
				}
				if (bestMatch.gameObject.GetDistance(gameObject) < T.gameObject.GetDistance(gameObject))
					bestMatch = T;	

			}
			if(bestMatch)
			{
				teleporterScript.connectedTeleporter = bestMatch.teleporterScript;
				bestMatch.teleporterScript = teleporterScript.connectedTeleporter;
			}

		}



	}*/

	void OnTriggerEnter2D(Collider2D col)
	{
		ITeleportable target = col.gameObject.GetComponent<ITeleportable>();
		if (target != null && CanTeleportObject(target))
		{

			InitiateTeleport(target, transform.WorldPositionToRelative(Collider.ClosestPoint(col.transform.position)));
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
		// teleport was not succesful (object backed out)
		if (TeleportingObjects.ContainsKey(col.gameObject))
		{
			AbortTeleport(TeleportingObjects[col.gameObject]);
			return;
		}

		if (TeleportingGhosts.ContainsKey(col.gameObject))
		{

			TeleportGhost ghostScript = TeleportingGhosts[col.gameObject];
			if (ghostScript.EnterTeleporter.gameObject != gameObject)
			{
				FinalizeTeleport(ghostScript.OriginalObject);
			}
			else
			{
				//ghostobject left teleporter initiator, this should not happen unless teleporters are next to each other
				Debug.LogError("strange thing happened");
				return;
			}

		}

	}

	private void OnValidate()
	{
		if (OtherTeleporter != null)
		{
			ITeleporter tele = OtherTeleporter.GetComponent<ITeleporter>();
			if (tele == null)
				return;
			ConnectTeleporter(tele);

		}

	}

	public bool ConnectTeleporter(ITeleporter tele)
	{
		ConnectedTeleporter = tele;
		if (ConnectedTeleporter.ConnectedTeleporter == null)
		{
			ConnectedTeleporter.ConnectTeleporter(this);
			var C = ConnectedTeleporter as Teleporter;
			C.OtherTeleporter = gameObject;
		}

		return true;
	}

	public bool CanTeleportObject(ITeleportable target)
	{
		if (target != null && target.CanBeTeleported() && CanBeEnteredFrom(target.transform.position))
		{
			if (Is2D)
			{
				if (target.transform.localPosition.z != transform.localPosition.z)
					return false;
				//if()
			}
			else
			{

			}

			return true;
		}
		return false;
	}

	public bool InitiateTeleport(ITeleportable obj, Vector3 enterPoint)
	{
		if (!(obj is ITeleportable))
			return false;
		GameObject ghostObj = Instantiate(obj.gameObject, ConnectedTeleporter.transform.position, obj.transform.rotation);
		// TODO: create chamelion script that will copy appearence instead of this
		MonoBehaviour[] scripts = ghostObj.GetComponents<MonoBehaviour>();
		for (int i = 0; i < scripts.Length; i++)
		{
			Destroy(scripts[i]);
		}
		TeleportGhost ghostScript = ghostObj.AddComponent<TeleportGhost>();
		ghostScript.Init(obj, enterPoint, this, ConnectedTeleporter);
		TeleportingObjects[obj.gameObject] = obj;

		ConnectedTeleporter.TeleportingGhosts[ghostObj] = ghostScript;

		return true;
	}

	public void FinalizeTeleport(ITeleportable obj)
	{
		obj.TeleportationGhost.FinalizeTeleport();

	}

	public void AbortTeleport(ITeleportable obj)
	{
		obj.TeleportationGhost.AbortTeleport();
	}

	void OnDrawGizmosSelected()
	{
		if (ConnectedTeleporter != null)
		{
			Gizmos.color = Color.cyan;
			Gizmos.DrawLine(gameObject.transform.position, ConnectedTeleporter.gameObject.transform.position);

		}
	}

	private void OnDestroy()
	{
		GameController.Teleporters.Remove(this);
	}

	public bool CanBeEnteredFrom(Vector3 position)
	{
		if (Vector3.Dot(FacingDirection, gameObject.GetDirection(position)) > EnterArc / 360 * -2 + 1)
			return true;
		return false;
	}
}
#if UNITY_EDITOR // conditional compilation is not mandatory
public class TeleporterConnection : EditorWindow
{
	bool Connecting = false;
	bool SelectedConected = false;

	public GameObject prevSelect;

	// Add menu named "My Window" to the Window menu
	[MenuItem("Window/Teleporter Connector")]
	static void Init()
	{
		// Get existing open window or if none, make a new one:
		TeleporterConnection window = (TeleporterConnection)EditorWindow.GetWindow(typeof(TeleporterConnection));
		window.Show();
		window.prevSelect = Selection.activeGameObject;
	}

	void OnGUI()
	{
		if (!Connecting && GUI.Button(new Rect(10, 10, 150, 40), "Start Connecting"))
		{
			Debug.Log("Select other gameobjects containing Tile_Teleporter script.");
			Connecting = true;
		}
		if (Connecting)
		{
			if (SelectedConected)
				if (GUI.Button(new Rect(10, 50, 150, 40), "Disconnect."))
				{
					SelectedConected = false;
					var thisTeleporter = prevSelect.GetComponent<ITeleporter>();
					var thatTeleporter = thisTeleporter.ConnectedTeleporter as ITeleporter;
					MonoBehaviour thisMB = thisTeleporter as MonoBehaviour;
					MonoBehaviour thatMB = thisTeleporter as MonoBehaviour;
					if (thisMB && thatMB)
					{
						Debug.Log("Disconnected tiles (" + thisMB.name + " and " + thatMB.name + ").");
					}

					thatTeleporter.ConnectedTeleporter = null;
					thisTeleporter.ConnectedTeleporter = null;
				}


			if (GUI.Button(new Rect(10, 10, 150, 40), "Cancel Connecting"))
				Connecting = false;
		}
	}
	void OnSelectionChange()
	{
		if (Selection.activeGameObject)
		{
			ITeleporter otherTeleporter = Selection.activeGameObject.GetComponent<ITeleporter>();
			if (!SelectedConected)
			{

				if (otherTeleporter != null)
				{
					ITeleporter ThisTeleporter = prevSelect.GetComponent<ITeleporter>();

					if (ThisTeleporter != null)
					{
						if (ThisTeleporter.ConnectTeleporter(otherTeleporter))
						{
							EditorUtility.SetDirty(ThisTeleporter as MonoBehaviour);
							EditorUtility.SetDirty(otherTeleporter as MonoBehaviour);
						}

						Debug.Log("Successfully connected.");

					}
					else
						Debug.Log("Conection failed.");
				}

			}
			prevSelect = Selection.activeGameObject;
			if (otherTeleporter.ConnectedTeleporter != null)
				SelectedConected = true;
			else
				SelectedConected = false;
			Repaint();
		}
	}
}
#endif