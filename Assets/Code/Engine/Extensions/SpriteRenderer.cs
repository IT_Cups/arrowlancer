﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionsSpriteRenderer
{
	public static void FadeSprite(this SpriteRenderer renderer, MonoBehaviour mono, float duration)
	{
		mono.StopCoroutine(UnfadeCoroutine(renderer, duration));
		mono.StartCoroutine(FadeCoroutine(renderer, duration));
	}

	public static void UnfadeSprite(this SpriteRenderer renderer, MonoBehaviour mono, float duration)
	{
		mono.StopCoroutine(FadeCoroutine(renderer, duration));
		mono.StartCoroutine(UnfadeCoroutine(renderer, duration));
	}

	private static IEnumerator FadeCoroutine(SpriteRenderer renderer, float duration)
	{
		// Fading animation
		float start = Time.time;
		Color color = renderer.color;
		while (Time.time <= start + duration)
		{
			color = renderer.color;
			color.a = 1f - Mathf.Clamp01((Time.time - start) / duration);
			renderer.color = color;
			yield return new WaitForEndOfFrame();
		}
	}
	private static IEnumerator UnfadeCoroutine(SpriteRenderer renderer, float duration)
	{
		// Fading animation
		float start = Time.time;
		Color color = renderer.color;
		while (Time.time <= start + duration)
		{
			color = renderer.color;
			color.a = Mathf.Clamp01((Time.time - start) / duration);
			renderer.color = color;
			yield return new WaitForEndOfFrame();
		}
	}
} 
