﻿using System.Collections.Generic;
using UnityEngine;

public static class ExtensionsTransform
{
	public static void RotateTowards2D(this Transform currentTransform, Vector3 targetDirection, float maxDegreesDelta)
	{
		float angle = Mathf.Atan2(targetDirection.y, targetDirection.x) * Mathf.Rad2Deg - 90;
		Quaternion q = Quaternion.LookRotation(Vector3.forward, targetDirection);

		currentTransform.rotation = Quaternion.RotateTowards(currentTransform.rotation, q, maxDegreesDelta);
	}

	public static Vector3 RelativePositionToWorld(this Transform currentTransform, Vector3 relativePos)
	{
		return currentTransform.TransformPoint(relativePos);
	}
	public static Vector3 WorldPositionToRelative(this Transform currentTransform, Vector3 relativePos)
	{
		return currentTransform.InverseTransformPoint(relativePos);
	}
	public static CopycatTeleporterTransform CreateCopyCat(this Transform currentTransform, GameObject originalObj, Vector3 enterPoint, Transform enterTele, Transform exitTele, bool keepRotation = true)
	{
		CopycatTeleporterTransform transCopyCat = currentTransform.gameObject.AddComponent<CopycatTeleporterTransform>();
		transCopyCat.OriginalObject = originalObj;
		transCopyCat.EnterPoint = enterPoint;
		transCopyCat.EnterTeleporter = enterTele;
		transCopyCat.ExitTeleporter = exitTele;
		transCopyCat.KeepRotation = keepRotation;
		return transCopyCat;
	}

}
