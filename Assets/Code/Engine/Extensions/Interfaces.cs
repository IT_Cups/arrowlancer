﻿using UnityEngine;

public static class ExtensionsInterfaces
{
	public static void FactionBasedBump(this IBumpable cur, IBumpable bumpedObject, Vector3 force, Vector3 point)
	{
		var otherFaction = bumpedObject as IFaction;
		var thisFaction = cur as IFaction;
		if(otherFaction == null || thisFaction == null)
		{
			Debug.LogError("One of the objects doesnt have IFaction interface.");
			return;
		}
		var standing = thisFaction.GetStanding(otherFaction);
		if (standing == 0)
		{
			bumpedObject.gameObject.GetComponent<Rigidbody2D>().AddForceAtPosition(force / 2, point, ForceMode2D.Impulse);
		}
		else if (standing < 0)
		{
			bumpedObject.gameObject.GetComponent<Rigidbody2D>().AddForceAtPosition(force, point, ForceMode2D.Impulse);
		}
		else
		{
			return;
		}
	}

} 
