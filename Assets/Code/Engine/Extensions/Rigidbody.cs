﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RigidbodyExtentions
{
	public static float KineticEnergy(this Rigidbody rb)
	{
		// mass in kg, velocity in meters per second, result is joules
		return 0.5f * rb.mass * rb.velocity.magnitude;
	}
}

public static class Rigidbody2DExtentions
{
	public static float KineticEnergy(this Rigidbody2D rb)
	{
		// mass in kg, velocity in meters per second, result is joules
		return 0.5f * rb.mass * rb.velocity.magnitude;
	}

	public static CopycatTeleporterRigidbody2D CreateCopyCat(this Rigidbody2D currentTransform, GameObject originalObj, Vector3 enterPoint, Transform enterTele, Transform exitTele, bool keepRotation = true)
	{
		CopycatTeleporterRigidbody2D transCopyCat = currentTransform.gameObject.AddComponent<CopycatTeleporterRigidbody2D>();
		transCopyCat.OriginalObject = originalObj;
		transCopyCat.EnterPoint = enterPoint;
		transCopyCat.EnterTeleporter = enterTele;
		transCopyCat.ExitTeleporter = exitTele;
		transCopyCat.KeepRotation = keepRotation;
		return transCopyCat;
	}
}
