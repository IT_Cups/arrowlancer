﻿using UnityEngine;


public static class ExtentionsVector
{
	// Vectors
	public static Vector3 Round(this Vector3 thisVector, int decimalPlaces = 2)
	{
		float multiplier = 1;
		for (int i = 0; i < decimalPlaces; i++)
		{
			multiplier *= 10f;
		}
		return new Vector3(
			Mathf.Round(thisVector.x * multiplier) / multiplier,
			Mathf.Round(thisVector.y * multiplier) / multiplier,
			Mathf.Round(thisVector.z * multiplier) / multiplier);
	}
	public static bool Approximately(this Vector3 thisVector, Vector3 otherVector)
	{
		if (Mathf.Approximately(thisVector.x, otherVector.x) && Mathf.Approximately(thisVector.y, otherVector.y) && Mathf.Approximately(thisVector.z, otherVector.z))
			return true;
		else
			return false;
	}


	public static Vector3 GetDirection(this Vector3 thisVector, Vector3 targetPosition)
	{
		return (targetPosition - thisVector).normalized;
	}
	public static Vector2 GetDirection(this Vector2 thisVector, Vector2 targetPosition)
	{
		return (targetPosition - thisVector).normalized;
	}

	public static Vector3 Random(this Vector3 myVector, Vector3 min, Vector3 max)
	{
		return  new Vector3(UnityEngine.Random.Range(min.x, max.x), UnityEngine.Random.Range(min.y, max.y), UnityEngine.Random.Range(min.z, max.z));
	}

	public static Vector3 RandomInCircle(this Vector3 myVector, float minRadius, float maxRadius)
	{
		return UnityEngine.Random.insideUnitCircle * UnityEngine.Random.Range(minRadius, maxRadius);
	}

}
