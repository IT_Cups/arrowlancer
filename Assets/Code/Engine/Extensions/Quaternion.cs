﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class QuaternionExtentions
{
	public static Vector3 ToDirection2D(this Quaternion thisQuaternion)
	{
		return thisQuaternion * Vector3.up;
	}
	public static Vector3 ToDirection(this Quaternion thisQuaternion)
	{
		return thisQuaternion * Vector3.forward;
	}

} 
