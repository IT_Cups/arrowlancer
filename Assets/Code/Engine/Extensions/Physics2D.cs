﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Physics2DExt
{
	public static RaycastHit2D Raycast2DExtended(Vector3 startingPos, Vector3 dir, float distance, List<Collider2D> ignoreColliders, int layers = Physics2D.DefaultRaycastLayers)
	{
		RaycastHit2D[] hits = Physics2D.RaycastAll(startingPos, dir, distance, layers);
		RaycastHit2D returnValue = new RaycastHit2D();
		foreach(var hit in hits)
		{
			if (!ignoreColliders.Contains(hit.collider))
				return hit;
		}
		returnValue.distance = distance;
		return returnValue;
	}
	public static bool CheckPathCollisions(Vector3 startingPos, Vector3 dir, float distance, List<Collider2D> ignoreColliders, Collider2D destinationCollider = null, int layers = Physics2D.DefaultRaycastLayers)
	{

		var hit = Physics2DExt.Raycast2DExtended(startingPos, dir, distance, ignoreColliders, layers);
		if (hit.collider != null)
		{
			if (destinationCollider && hit.collider == destinationCollider)
				return false;
			else
				return true;
		}
			

		return false;
	}
}
