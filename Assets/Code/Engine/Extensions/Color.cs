﻿using UnityEngine;

public static class ExtensionsColor
{
	public enum ColorBlendMode
	{
		Normal,
		Screen,
		Multiply
	}
	// Direction
	public static Color Blend(this Color cur, Color add, ColorBlendMode mode = ColorBlendMode.Normal)
	{
		float finalAlpha = 1 - add.a + cur.a;
		switch (mode)
		{
			case ColorBlendMode.Normal:
				{
					return cur * finalAlpha + add * add.a;
				}
			// not tested
			case ColorBlendMode.Screen:
				{
					return ((cur * finalAlpha).Invert()) + ((add * add.a).Invert()).Invert();
				}
			// not tested
			case ColorBlendMode.Multiply:
				{
					return (cur * finalAlpha) * (add * add.a);
				}
		}
		return cur;
	}

	public static Color[] Blend(this Color[] thisColors, Color[] addColors, ExtensionsColor.ColorBlendMode mode = ColorBlendMode.Normal)
	{
		Color[] finalColors = thisColors;
		for (int index = 0; index < thisColors.Length; index++)
		{
			Color pixel = addColors[index];

			if (pixel.a == 1)
				finalColors[index] = pixel;
			else if (pixel.a != 0)
				finalColors[index] = finalColors[index].Blend(pixel, mode);
		}
		return finalColors;
	}
	public static Color[] Replace(this Color[] thisColors, Color[] replaceColors)
	{
		Color[] finalColors = thisColors;

		for (int index = 0; index < thisColors.Length; index++)
		{ 
			Color replacePixel = replaceColors[index];
			if (replacePixel.a > 0)
				thisColors[index] = replacePixel;
		}
		return finalColors;
	}

	public static Color Invert(this Color cur)
	{
		return new Color(1.0f - cur.r, 1.0f - cur.g, 1.0f - cur.b, cur.a);
	}

} 
