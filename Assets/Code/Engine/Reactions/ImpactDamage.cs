﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpactDamage : MonoBehaviour
{
	public bool DestroyOnImpact = true;
	public float Damage = 1;
	void OnCollisionEnter2D(Collision2D collision)
	{
		IDamagable target = collision.gameObject.GetComponent<IDamagable>();
		if(target != null)
		{
			target.TakeDamage(Damage);
			if (DestroyOnImpact)
				Destroy(gameObject);
		}
		
	}
}
