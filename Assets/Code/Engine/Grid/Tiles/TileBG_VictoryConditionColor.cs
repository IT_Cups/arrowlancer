﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileBG_VictoryConditionColor : TileBG, ICondition
{
    public Color RequiredColor;
    private bool isMetFlag = false;

	private List<Tile_Draggable> CollidedTiles = new List<Tile_Draggable>();
	private float brightness = 0.5f;


    private void OnValidate()
    {
		SpriteRenderer.color = RequiredColor * brightness;

	}
    public void AfterLostActions()
    {
		DimDown();

	}

    public void AfterMetActions()
    {
		LightUp();
		IVictoryController controller = ParentGrid as IVictoryController;
		if (controller != null)
            controller.CheckVictoryConditions();
    }

	public void LightUp()
	{
		if (brightness >= 0.8f)
			return;
		StopCoroutine(DimDownAnimation());
		StartCoroutine(LightUpAnimation());
	}

	public void DimDown()
	{
		if (brightness <= 0.5f)
			return;
		StopCoroutine(LightUpAnimation());
		StartCoroutine(DimDownAnimation());
	}

	public override void CellContentChanged(TileBase culprit = null)
    {
        var beforeIsMetFlag = isMetFlag;
		isMetFlag = IsMet();
        if(beforeIsMetFlag != isMetFlag)
        {
            if (isMetFlag == true)
                AfterMetActions();
            else if (isMetFlag == false)
                AfterLostActions();
        }    
           
    }

    public bool IsMet()
    {
		foreach (var tile in CollidedTiles)
		{
			if (tile.gameObject.GetDistance(gameObject) <= 0.5f && tile.GetColor() == RequiredColor)
				return true;
		}
        return false;
    }

	void OnCollisionEnter2D(Collision2D collision)
	{
		var tile = collision.gameObject.GetComponent<Tile_Draggable>();
		if (tile)
			CollidedTiles.Add(tile);
	}

	void OnCollisionExit2D(Collision2D collision)
	{
		var tile = collision.gameObject.GetComponent<Tile_Draggable>();
		if (tile)
			CollidedTiles.Remove(tile);
	}
	protected override void Update()
	{
		base.Update();
		if(IsMet())
			LightUp();
		else
			DimDown();
	}
	private IEnumerator LightUpAnimation()
	{
		var targetBrightness = 0.8f;
		var step = targetBrightness / 40;
		if (brightness >= targetBrightness)
		{
			yield break;
		}
		brightness += step;
		SpriteRenderer.color = RequiredColor * brightness;

		yield return new WaitForSeconds(0.4f);
	}
	private IEnumerator DimDownAnimation()
	{
		var targetBrightness = 0.5f;
		var step = targetBrightness / 40;
		if (brightness <= targetBrightness)
		{
			yield break;
		}
		brightness -= step;
		SpriteRenderer.color = RequiredColor * brightness;
		
		yield return new WaitForSeconds(0.4f);
	}
}
