﻿using UnityEngine;

public class TileBG_SimpleColorVarity : TileBG
{

	protected override void Awake()
	{
		base.Awake();
		float k = Random.Range(0.1f,0.2f);
		SpriteRenderer.color = new Color(k,k,k);
	}

}
