﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class TileBase : MonoBehaviour, IMovable, ITiles.ITile
{

	public GridControl ParentGrid { get; set; }

	private int lastMovement = 0;
	public bool IsMoving { get => Time.frameCount - lastMovement > 1 ? true : false; }

	public virtual bool Redirects { get; set; } = false;

	protected MovementPattern movementPattern;

	private SpriteRenderer spriteRenderer;

	public SpriteRenderer SpriteRenderer {
		get => spriteRenderer ? spriteRenderer : spriteRenderer = GetComponent<SpriteRenderer>();
		set => spriteRenderer = value;
	}
	

	// Start is called before the first frame update
	protected virtual void Awake()
    {
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	// Update is called once per frame
	protected virtual void Update()
    {

	}

	protected virtual void Reset()
	{
	
	}

	// returns cell postiton of connected tile, null if already connected
	// if stealthy is true connecting tile wont trigger CellContentChanged() for every tile in that cell
	public virtual Vector3Int? ConnectToGrid(GridControl grid, bool stealthy = false)
	{
		return grid.ConnectTile(this, stealthy);
	}

	public virtual bool DisconnectFromGrid()
	{
		return ParentGrid.DisconnectTile(this);
	}

	public virtual void CellContentChanged(TileBase culprit = null)
	{
		return;
	}

	public virtual Vector3Int? FinishedMovement()
	{
		// RESUME: SNAP TO GRID (consider adapting to multitile tiles)
		var newPos = ParentGrid.UpdateTilePosition(this);
		if(newPos != null)
		{

		}
		return newPos;
	}


	public virtual Vector3 Move(Vector3 direction, float maxSpeed)
	{
		if (direction.magnitude == 0)
			return Vector3.zero;
		Vector3 lastPos = gameObject.transform.position;

		gameObject.transform.position = gameObject.transform.position + direction.normalized * Mathf.Min(maxSpeed * Time.smoothDeltaTime, direction.magnitude);
		lastMovement = Time.frameCount;
		return gameObject.transform.position - lastPos;
	}

	public virtual Vector3Int GetInGridPosition()
	{
		return ParentGrid.GetTilePosition(this).Value;
	}

	public virtual Vector3 GetInGridWorldPosition()
	{
		Vector3Int gridPos = ParentGrid.GetTilePosition(this).Value;
		Vector3 pos = new Vector3(gridPos.x + ParentGrid.grid.cellSize.x / 2, gridPos.y + ParentGrid.grid.cellSize.y / 2, transform.position.z);
		return ParentGrid.grid.LocalToWorld(pos);
	}

	// if force is true will return tiles like teleporters instead of tiles on teleporter exits
	public virtual T GetAdjacentTile<T>(ref Axis axis, ref Direction direction, bool ignoreRedirect = false) where T : TileBase
	{
		List<T> suitableTiles = GetAdjacentTiles<T>(axis, direction);
		T returnTile = null;
		
		if (suitableTiles != null)
		{
			if (suitableTiles.Count > 1)
			{
				Debug.LogError("Multiple tiles in adjacent pos.You probably should resolve that.");
			}
			returnTile = suitableTiles[0];
		}

		if(!ignoreRedirect)
		{
			if (returnTile && returnTile.Redirects)
			{
				returnTile = returnTile.GetAdjacentTile<T>(ref axis, ref direction, ignoreRedirect);
			}
			// let check if next tile redirects to T type
			else if (!returnTile)
			{
				var adjacentTiles = GetAdjacentTiles<TileBase>(axis, direction);
				if (adjacentTiles == null)
					return null;
				bool error = false;
				// lets find any redirecting tile that not redirects to this tile
				foreach (var tile in adjacentTiles)
				{
					if(tile.Redirects)
					{
						if (returnTile)
						{
							error = true;
							break;
						}	
						var t = tile.GetAdjacentTile<T>(ref axis, ref direction, ignoreRedirect);
						if (t && t != this)
							returnTile = t;
					}
				}
				if (error)
				{
					Debug.LogError("Multiple tiles that redirects.You probably should resolve that.");
				}
			}
		}
		return returnTile;
	}

	/// <summary>
	/// Get tiles adjacent to this tile.
	/// <para>Does not supports redirects.</para>
	/// </summary>
	public virtual List<T> GetAdjacentTiles<T>(Axis axis, Direction direction) where T : TileBase
	{
		Vector3Int? curPos = GetInGridPosition();
		List<T> returnTiles = null;
		switch (axis)
		{
			case Axis.X:
				returnTiles = ParentGrid.GetTiles<T>(new Vector3Int(curPos.Value.x + Math.Sign((int)direction), curPos.Value.y, curPos.Value.z));
				break;

			case Axis.Y:
				returnTiles = ParentGrid.GetTiles<T>(new Vector3Int(curPos.Value.x, curPos.Value.y + Math.Sign((int)direction), curPos.Value.z));
				break;
		}

		return returnTiles;
	}

	public virtual T GetAdjacentTile<T>(Axis axis, Direction direction, bool ignoreRedirect = false) where T : TileBase
	{
		return GetAdjacentTile<T>(ref axis, ref direction, ignoreRedirect);
	}

	public virtual MovementError CanMove()
	{
		MovementError returnValue = MovementError.None;
		if (IsMoving)
			returnValue |= MovementError.BusyMoving;
		return returnValue;
	}

	public virtual void Stop()
	{
		movementPattern = null;
	}

	public virtual void SnapToGrid()
	{
		Vector3 newPosition = ParentGrid.grid.LocalToCell(transform.localPosition);
		var margin = ParentGrid.grid.cellSize / 2;
		margin.z = 0;
		newPosition += margin;
		transform.localPosition = newPosition;
	}

}
