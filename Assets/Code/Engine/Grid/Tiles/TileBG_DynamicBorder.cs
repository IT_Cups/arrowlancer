﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileBG_DynamicBorder : TileBG, ICosmetic, IDynamicSprite
{
	private DynamicTexture_SideConditions generator;
	public DynamicTexture_SideConditions Generator
	{
		get
		{
			if (generator)
				return generator;
			generator = GetComponent<DynamicTexture_SideConditions>();
			if (generator)
				return generator;
			generator = GetComponentInParent<DynamicTexture_SideConditions>();
			if (generator)
				return generator;
			else
			{
				Debug.LogError("No dynamic texture script found.");
				return null;
			}
		}
		set => generator = value;
	}

	public void GenerateDynamicSprite(bool supressMessage = false)
	{
		Generator.GenerateSprite(supressMessage);
	}
}
