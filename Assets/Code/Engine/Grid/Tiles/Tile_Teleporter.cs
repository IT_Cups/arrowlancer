﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
/*
public class Tile_Teleporter : TileBase, ITeleporter, IDynamicSprite
{
	public Vector3 FacingDirection { get => transform.rotation.eulerAngles; set { transform.rotation = Quaternion.Euler(value); } }

	[SerializeField] private Tile_Teleporter connectedTeleporterTile;

	public ITeleporter ConnectedTeleporter { get => connectedTeleporterTile; set { connectedTeleporterTile = value as Tile_Teleporter; } }
	public Dictionary<GameObject, ITeleportable> TeleportingObjects { get; set; }
	public Dictionary<GameObject, TeleportGhost> TeleportingGhosts { get; set; }

	[HideInInspector]
	public override bool Redirects { get; set; } = true;

	public int TeleGHosts = 0;
	public int TeleObjs = 0;

	public bool staticTeleporter = true;
	private DynamicTexture_SideConditions generator;
	public DynamicTexture_SideConditions Generator
	{
		get => generator? generator : generator = GetComponentInChildren<DynamicTexture_SideConditions>();
		set => generator = value;
	}

protected override void Awake()
	{
		base.Awake();
		TeleportingObjects = new Dictionary<GameObject, ITeleportable>();
		TeleportingGhosts = new Dictionary<GameObject, TeleportGhost>();

		Color c = Camera.main.backgroundColor;
		c.a = 1f;
		SpriteRenderer.color = c;
		//spriteRenderer.sprite = null;

		if (!connectedTeleporterTile)
			Debug.LogError("TileTeleporter (" + gameObject.name+ ") doesn't have connected teleporter.");
	}

	protected override void Update()
	{
		TeleGHosts = TeleportingGhosts.Count;
		TeleObjs = TeleportingObjects.Count;
	}


	void OnCollisionEnter2D(Collision2D collision)
	{
		ITeleportable target = collision.gameObject.GetComponent<ITeleportable>();
		if (target != null && target.CanBeTeleported() && CanTeleportObject(target.gameObject))
		{
			InitiateTeleport(target, collision.GetContact(0).point); //changed, need test
		}		
	}

	private Vector3 SideToEnterPoint(Direction side)
	{
		switch(side)
		{
			case Direction.Left:
				return new Vector3(-0.5f, 0);
			case Direction.Right:
				return new Vector3(0.5f, 0);
			case Direction.Top:
				return new Vector3(0, 0.5f);
			case Direction.Bottom:
				return new Vector3(0, -0.5f);
			default:
				Debug.LogError("enter point side error");
				return Vector3.zero;

		}
	}
	
	void OnCollisionExit2D(Collision2D collision)
	{
		// if object is teleported that means a ghost left collision
		ITeleportable teleported = FinalizeTeleport(collision.gameObject);
		if (teleported != null)
		{
			Tile_Draggable tileScript = teleported as Tile_Draggable;
			if(tileScript)
			{
				if (FacingDirection.IsSameAxisAs(connectedTeleporterTile.FacingDirection))
				{
					if(FacingDirection == connectedTeleporterTile.FacingDirection)
					{
						tileScript.SetDragRules(null, null, !tileScript.Inversed);
					}
				}
				else
				{
					if(FacingDirection.GetAxis() == Axis.X)
					{
						if((FacingDirection > 0 && connectedTeleporterTile.FacingDirection > 0) || (FacingDirection < 0 && connectedTeleporterTile.FacingDirection < 0))
							tileScript.SetDragRules(null, Axis.X, !tileScript.Inversed);
						else
							tileScript.SetDragRules(null, Axis.X, null);
					}
					else
					{
						if ((FacingDirection > 0 && connectedTeleporterTile.FacingDirection > 0) || (FacingDirection < 0 && connectedTeleporterTile.FacingDirection < 0))
							tileScript.SetDragRules(null, Axis.Y, !tileScript.Inversed);
						else
							tileScript.SetDragRules(null, Axis.Y, null);
					}
				}
			}
		}

	}
	

	public override T GetAdjacentTile<T>(ref Axis axis, ref Direction direction, bool ignoreRedirect = false)
	{
		if (ignoreRedirect)
			return base.GetAdjacentTile<T>(ref axis, ref direction, ignoreRedirect);
		else
		{
			if (FacingDirection == connectedTeleporterTile.FacingDirection)
				direction = direction.GetOpposite();
			else if (!FacingDirection.IsOpposite(connectedTeleporterTile.FacingDirection))
				switch (axis)
				{
					case Axis.X:
						axis = Axis.Y;
						break;
					case Axis.Y:
						axis = Axis.X;
						break;
				}
			return connectedTeleporterTile.GetAdjacentTile<T>(ref axis, ref direction, true);
		}
		

	}
	private void OnValidate()
	{
		switch (FacingDirection)
		{
			case Direction.Bottom:
				gameObject.transform.rotation = Quaternion.Euler(0, 0, 270);
				break;
			case Direction.Left:
				gameObject.transform.rotation = Quaternion.Euler(0, 0, 180);
				break;
			case Direction.Top:
				gameObject.transform.rotation = Quaternion.Euler(0, 0, 90);
				break;
			case Direction.Right:
				gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
				break;
			case Direction.None:
				Debug.LogError("Cant be none");
				FacingDirection = Direction.Left;
				break;
		}
		Generator.gameObject.transform.localRotation = Quaternion.Euler(-transform.rotation.eulerAngles);
		if (connectedTeleporterTile)
		{
			ConnectTeleporter(connectedTeleporterTile);
			
		}
		
	}

	public override MovementError CanMove()
	{
		if (staticTeleporter)
			return MovementError.Immobile;
		else
			return base.CanMove();
	}

	public bool ConnectTeleporter(ITeleporter tele)
	{
		connectedTeleporterTile = tele as Tile_Teleporter;
		if (connectedTeleporterTile.ConnectedTeleporter == null)
		{
			connectedTeleporterTile.ConnectTeleporter(this);
		}

		return true;
	}

	public void ProcessTeleporting()
	{
		foreach (var entry in TeleportingGhosts)
		{
			entry.Value.UpdatePosition();
		}
	}

	public bool CanTeleportObject(GameObject obj)
	{
		ITeleportable target = obj.GetComponent<ITeleportable>();
		if (target != null && gameObject.GetSide(target.gameObject) == FacingDirection)
		{
			if (target.transform.localPosition.z != transform.localPosition.z)
				return false;
			Tile_Draggable tileScript = target as Tile_Draggable;
			if (tileScript && tileScript.DirectionOfMovement != FacingDirection.GetOpposite())
				return false;

			if (FacingDirection.GetAxis() == Axis.X)
			{
				if (Mathf.Abs(gameObject.transform.position.y - tileScript.transform.position.y) < 0.2f)
					return true;
			}
			if (FacingDirection.GetAxis() == Axis.Y)
			{
				if (Mathf.Abs(gameObject.transform.position.x - tileScript.transform.position.x) < 0.2f)
					return true;
			}
		}
		return false;
	}

	public bool InitiateTeleport(ITeleportable obj, Vector3 enterPoint)
	{
		if (!(obj is ITeleportable))
			return false;
		GameObject ghostObj = Instantiate(obj.gameObject);
		// TODO: create chamelion script that will copy appearence instead of this
		MonoBehaviour[] scripts = ghostObj.GetComponents<MonoBehaviour>();
		for (int i = 0; i < scripts.Length; i++)
		{
			Destroy(scripts[i]);
		}
		TeleportGhost ghostScript = ghostObj.AddComponent<TeleportGhost>();
		ghostScript.Init(obj, enterPoint, this, connectedTeleporterTile);
		TeleportingObjects[obj.gameObject] = obj;

		connectedTeleporterTile.TeleportingGhosts[ghostObj] = ghostScript;

		ghostScript.UpdatePosition();
		return true;
	}

	public ITeleportable FinalizeTeleport(GameObject obj)
	{
		TeleportGhost ghostScript;
		// teleport was not succesful (object backed out)
		if (TeleportingObjects.ContainsKey(obj))
		{
			ghostScript = TeleportingObjects[obj].TeleportationGhost;
			return ghostScript.FinalizeTeleport(true);
		}

		if (TeleportingGhosts.ContainsKey(obj))
		{
			ghostScript = TeleportingGhosts[obj];
			if (ghostScript.EnterTeleport.gameObject == gameObject)
			{
				//ghostobject left teleporter initiator, this should not happen unless teleporters are next to each other
				Debug.LogError("strange thing happened");
				return null;
			}

			return ghostScript.FinalizeTeleport();

		}
		return null;
	}

	void OnDrawGizmosSelected()
	{
		if (ConnectedTeleporter != null)
		{
			Gizmos.color = Color.cyan;
			Gizmos.DrawLine(gameObject.transform.position, ConnectedTeleporter.gameObject.transform.position);

		}
	}

	public void GenerateDynamicSprite(bool supressMessage = false)
	{
		
		if (staticTeleporter)
			Generator.GenerateSprite(supressMessage);
	}
}
*/