﻿using UnityEngine;
using System.Collections;
[System.Flags]

public enum MovementError : uint
{
	None = 0,
	BusyMoving = 1 << 1,
	Immobile = 1 << 2,
	StopsOthers = 1 << 3
}

public interface IInteractable : IGameObject
{

}
public interface IMovable : IGameObject
{
	bool IsMoving { get; }
	MovementError CanMove();
	Vector3 Move(Vector3 direction, float maxSpeed);
	void Stop();
}

public interface IDamagable
{
	/// <summary>
	/// Maximum entity health.
	/// </summary>
	float MaxHealth { get; set; }
	/// <summary>
	/// Current entity health.
	/// </summary>
	float Health { get; set; }
	/// <summary>
	/// Actions that are called when entity health becomes zero.
	/// </summary>
	void OnDeath();
	/// <summary>
	/// Process taking damage.
	/// </summary>
	void TakeDamage(float amount);
}
public interface ITeleportable : IGameObject
{
	TeleportGhost TeleportationGhost { get; set; }
	bool IsBeingTeleported { get; }
	bool CanBeTeleported();
}
public interface IHideble : IGameObject
{
	/// <summary>
	/// Bool property that indicates whether element shown or not.
	/// </summary>
	bool IsHidden { get; }
	/// <summary>
	/// Hides element from view.
	/// </summary>
	void Hide();
	/// <summary>
	/// Shows element.
	/// </summary>
	void Show();
}

public interface IBumpable : IGameObject
{
	bool CanBump(IBumpable bumpedObject);
	void Bump(IBumpable bumpedObject, Vector3 force, Vector3 point);
}

public interface IFaction
{
	string FactionName { get; }
	int GetStanding(IFaction faction);
	Faction FactionData{ get; }
}
public interface ITargeting
{
	IGameObject CurrentTarget { get; }
	IGameObject GetTarget();
}

public static class ITargetingExtentions
{
	public static (IGameObject, Vector3)? FindTargetFactionBased(this ITargeting thisTargeting, float maxDistance)
	{
		IGameObject target = null;
		Vector3 direction = Vector3.zero;
		IFaction thisFaction = thisTargeting as IFaction;
		GameObject thisGameObject = (thisTargeting as IGameObject).gameObject;
		Collider2D thisCollider = (thisTargeting as ICollider2D).Collider;
		if (thisFaction == null)
		{
			Debug.LogError("Cant use FindTargetFactionBased without Faction interface");
			return null;
		}
			
		float shortestDist = -1;
		foreach (var mob in GameController.Mobs)
		{
			if (thisFaction.FactionName == "" || thisFaction.FactionData.Standings.Length == 0)
			{
				Debug.LogError("No Faction data for " + thisGameObject.name + ". Cannot search for target.");
				return null;
			}
			IFaction faction = mob as IFaction;
			if (faction != null && thisFaction.GetStanding(faction) < 0)
			{
				ICollider2D otherCollider = mob as ICollider2D;
				if (otherCollider != null)
				{
					var path = TeleporterExtention.FindShortestStraightPathTo(thisGameObject.transform.position, mob.transform.position, maxDistance, thisCollider, otherCollider.Collider);

					if (path.HasValue)
					{
						var dist = path.Value.magnitude;
						if (shortestDist == -1 || shortestDist > dist)
						{
							shortestDist = dist;
							target = mob;
							direction = path.Value;
						}

					}
				}


			}

		}
		return (target, direction);
	}
}

public interface IGuidable
{
	void GuideTo(IGameObject target);
}

