﻿using UnityEngine;
using System;
using System.Collections.Generic;

public interface ICosmetic : IGameObject
{

}


public interface IGameObject
{
#pragma warning disable IDE1006 // Naming Styles
	GameObject gameObject { get; }
	Transform transform { get; }
#pragma warning restore IDE1006 // Naming Styles
}

public interface ICollider2D
{
	Collider2D Collider { get; }
}

namespace ITiles
{
	public interface ITile : IGameObject
	{

		GridControl ParentGrid { get; set; }
		Vector3Int GetInGridPosition();
		Vector3 GetInGridWorldPosition();
		T GetAdjacentTile<T>(ref Axis axis, ref Direction direction, bool ignoreRedirect = false) where T : TileBase;


	}

	public interface ITileDraggable : IGameObject
	{

		Axis MovementAxis { get; }
		Direction DirectionOfMovement { get; }
		bool Inversed { get; }
		TileDrag MasterTileDrag { get; }

		void DragMoveTo(float distance, float maxSpeed);

		void SetDragRules(TileDrag master, Axis? axis, bool? isInversed);

	}
}

public interface ICondition : IGameObject
{
	/// <summary>
	/// Returns true if conditions are fulfilled.
	/// </summary>
	bool IsMet();
	/// <summary>
	/// Actions that should be taken after conditions are met.
	/// </summary>
	void AfterMetActions();
	/// <summary>
	/// Actions that should be taken after conditions are lost.
	/// </summary>
	void AfterLostActions();

}
public interface ICopyCat : IGameObject
{
	/// <summary>
	/// Reference to original object this copycat script trying to copy
	/// </summary>
	GameObject OriginalObject { get; set; }
}
public interface IDynamicSprite : IGameObject
{
	void GenerateDynamicSprite(bool supressMessage = false);
}
public interface IProjectile : IGameObject
{

	/// <summary>
	/// Current spped of the projectile.
	/// </summary>
	float CurrentSpeed { get; set; }
	/// <summary>
	/// Current direction that projectile faces.
	/// </summary>
	Vector3 CurrentDirection { get; set; }

}
public interface IProjectileLifeTime : IProjectile
{
	float LifeTimeLeft { get; set; }
}
public interface IProjectileLifeDistance : IProjectile
{
	float LifeDistanceLeft { get; set; }

}
public interface ITeleporter : IGameObject
// use TeleporterDive or simular with this
{

	Dictionary<GameObject, ITeleportable> TeleportingObjects { get; set; }
	Dictionary<GameObject, TeleportGhost> TeleportingGhosts { get; set; }

	Vector3 FacingDirection { get; set; }
	ITeleporter ConnectedTeleporter { get; set; }
	bool CanBeEnteredFrom(Vector3 position);
	bool ConnectTeleporter(ITeleporter tele);
	bool CanTeleportObject(ITeleportable obj);
	bool InitiateTeleport(ITeleportable obj, Vector3 enterPoint);
	void FinalizeTeleport(ITeleportable obj);
	void AbortTeleport(ITeleportable obj);
}

public interface ITeleporter2D : ITeleporter
{
	Collider2D Collider { get;}
}

	public interface IUIElement : IHideble
{
	string id { get; }

}

public interface IProgress
{
	float Progress { get; }

	void SetProgress(float percent, bool triggerActions = true);

	event Action OnComplete;



}
public interface IVictoryController
{
	/// <summary>
	/// List of conditions that needs to be fulfilled before victory can be achieved.
	/// </summary>
	List<ICondition> VictoryConditions { get; set; }
	/// <summary>
	/// Actions that should be taken after all conditions are met.
	/// </summary>
	void VictoryActions();
	/// <summary>
	/// Check conditions and calls <seealso cref="VictoryActions()"/> if all conditions are met.
	/// </summary>
	void CheckVictoryConditions();

}

public interface IPlayer : IMob
{
	
}

public interface IMob : IGameObject
{

}


