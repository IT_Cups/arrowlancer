﻿using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyTarget : MonoBehaviour, IDamagable, ICondition
{

	public float Health { get; set; }
	[SerializeField]
	[Range(0, 999999)]
	private float maxHealth = 1;
	public float MaxHealth { get => maxHealth; set => maxHealth = value; }

	private void Start()
	{
		Health = MaxHealth;
	}
	public void AfterLostActions()
	{
		throw new System.NotImplementedException();
	}

	public void AfterMetActions()
	{
		IVictoryController controller = Camera.main.GetComponent<IVictoryController>();
		if (controller != null)
			controller.CheckVictoryConditions();
	}

	public bool IsMet()
	{
		if (Health > 0)
			return false;
		else
			return true;
	}

	public void OnDeath()
	{
		AfterMetActions();
	}

	public void TakeDamage(float amount)
	{
		Health -= amount;
		if(Health <= 0)
		{
			OnDeath();
			Destroy(gameObject);
		}
	}

}
