﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DullChaser : BumpyMob
{
	private IMob chaseTarget;
	private Vector2? selectedPath;
	public Collider2D TargetCollider { get; set; }
	public float MaxDistanceToTarget = 10;
	private float distance = 0;
	public float MaxSpeed = 1;
	// Start is called before the first frame update
	protected override void Start()
    {
		base.Start();
		StartCoroutine(FindingPath());

	}
	protected override void Awake()
	{
		base.Awake();
		chaseTarget = GameController.Players[GameController.PlayerNumber.Player1].Avatar as IMob;
		TargetCollider = chaseTarget.gameObject.GetComponent<Collider2D>();
		Collider = GetComponent<Collider2D>();
	}

	// Update is called once per frame
	protected override void Update()
    {
		base.Update();
		
		if(selectedPath.HasValue)
		{
			if (distance <= 0)
			{
				Stop();
				return;
			}
			distance -= Move(selectedPath.Value.normalized, MaxSpeed).magnitude;
		}
    }

	protected override void OnCollisionEnter2D(Collision2D collision)
	{
		base.OnCollisionEnter2D(collision);

	}

	public override void Bump(IBumpable bumpedObject, Vector3 force, Vector3 point)
	{
		Debug.Log("Bumped");
		Stop();
		base.Bump(bumpedObject,force,point);
		
	}

	public override void Stop()
	{
		selectedPath = null;
		base.Stop();
		StartCoroutine(FindingPath());
	}
	private IEnumerator FindingPath()
	{
		while(true)
		{
			yield return new WaitForSeconds(1f);

			if (!selectedPath.HasValue)
			{
				selectedPath = TeleporterExtention.FindShortestStraightPathTo(transform.position, chaseTarget.transform.position, MaxDistanceToTarget, Collider, TargetCollider);
				
				if (selectedPath.HasValue)
				{
					DebugHelper.DrawDebugArrow(transform.position, (Vector2)selectedPath, Color.green, duration: 0.5f);
					distance = selectedPath.Value.magnitude;
				}
				else
				{
					DebugHelper.DrawDebugArrow(transform.position, transform.position.GetDirection(chaseTarget.transform.position) * MaxDistanceToTarget, Color.red, duration: 0.5f);
				}
			}
			else
				StopCoroutine(FindingPath());
		}
	}
	void OnDrawGizmosSelected()
	{
		if (chaseTarget != null)
		{
			Gizmos.color = Color.yellow;
			Gizmos.DrawSphere(chaseTarget.transform.position, 0.5f);
		}
	}
}
