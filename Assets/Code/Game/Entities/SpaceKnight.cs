﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

using UnityEngine.InputSystem.Gestures;
using UnityEngine.InputSystem.PointerManager;
using UnityEngine.UI;
using UnityEngine.InputSystem.Interactions;


public class SpaceKnight : BumpyMob, IPlayer
{
	

	public GameObject ProjectilePrefab;
	public GestureManager Gesture;
	public PointerInputManager PointerManager;
	[Header("Attrivutes")]
	[Header("Weapon")]
	public float WeaponChargeStep = 2;
	public float ProjectileMaxSpeed = 2;
	public float ProjectileLifeTime = 1f;
	public float ProjectileRecoil = 1f;
	public float ProjectileSlowdown = 1f;
	public float WeaponEnergyConsumtion = 1;

	public float NoChargeRadius = 1;
	public float MaxChargeRadius = 3;
	[Header("Movement")]
	public float DashCooldown = 2f;
	public float DashSpeed = 0.5f;

	[Header("Energy")]
	[HideInInspector]
	public float Energy;
	public float MaxEnergy = 100;
	public float EnergyRegeneration = 4;

	public Vector2 CurrentMoveImpulse = Vector2.zero;


	private KnightControls Controls;
	public Charger_Arrow charger;
	private ProgressDial DashCD;
	// think if glabal ui controller is needed here and is it better to just set up public var for this VVV
	private DualSliderControl energyBar;

	private Rigidbody2D rigid;

	private bool charging = false;
	private Vector2 chargeStartPos;
	



	protected override void Awake()
	{
		base.Awake();
		rigid = GetComponent<Rigidbody2D>();

		Controls = new KnightControls();

		if (!GameController.Players.ContainsKey(GameController.PlayerNumber.Player1))
		{
			GameController.RegisterPlayer(GameController.PlayerNumber.Player1, this, PointerManager, Gesture);
		}


	}

	protected override void Start()
	{
		base.Start();
		DashCD = UIController.Elements["DashCD"] as ProgressDial;
		energyBar = UIController.Elements["SK_Energy"] as DualSliderControl;
		SetEnergy(MaxEnergy);
		DashCD.Progress = 0;
		DashCD.OnComplete += DashCD.Hide;
		DashCD.Hide();


	}
	// Update is called once per frame
	protected override void Update()
	{
		base.Update();
		if (charging)
		{

			var mousePos = GetPointerPos();
			var RelativeToPointer = mousePos - chargeStartPos;

			if (RelativeToPointer.magnitude < NoChargeRadius && charger.Progress > 0)
				charger.SetProgress(0);
			// if controls are within max charge range
			else if (RelativeToPointer.magnitude >= NoChargeRadius && RelativeToPointer.magnitude <= MaxChargeRadius)
			{
				var chargeFactor = Mathf.InverseLerp(NoChargeRadius, MaxChargeRadius, RelativeToPointer.magnitude);
				if (Energy < WeaponEnergyConsumtion)
				{
					var energyFactor = Mathf.InverseLerp(0, WeaponEnergyConsumtion, Energy);
					chargeFactor *= energyFactor;

				}
				charger.SetProgress(chargeFactor);
			}
			// if controls are further from max charge range
			else if (RelativeToPointer.magnitude > MaxChargeRadius && charger.Progress != 1)
			{
				var chargeFactor = 1f;
				if (Energy < WeaponEnergyConsumtion)
				{
					var energyFactor = Mathf.InverseLerp(0, WeaponEnergyConsumtion, Energy);
					chargeFactor *= energyFactor;

				}
				charger.SetProgress(chargeFactor);
			}

			charger.Direction = -RelativeToPointer.normalized;

		}

		SetEnergy(Energy + EnergyRegeneration * Time.deltaTime);


	}

	protected virtual void OnEnable()
	{

		Controls.Enable();
		Controls.Player.ChargingAttack.performed += ChargingAttack;
		Controls.Player.Fire.performed += Fire;
		Controls.Player.Pause.performed += Pause;

		Gesture.MultiTapped += OnDoubleTap;
	}

	private void OnDisable()
	{
		Controls.Disable();
		Controls.Player.ChargingAttack.performed -= ChargingAttack;
		Controls.Player.Fire.performed -= Fire;
		Controls.Player.Pause.performed -= Pause;

		Gesture.MultiTapped -= OnDoubleTap;
	}

	public void SetEnergy(float v)
	{
		Energy = Mathf.Min(Mathf.Max(0, v), MaxEnergy);
		energyBar.SetSliderValues(Energy / MaxEnergy);
	}


	public void Fire(InputAction.CallbackContext action)
	{
		if (action.performed)
		{

			if(charging && charger.Progress > 0)
			{
				var arrow = Instantiate(ProjectilePrefab, transform.position, charger.transform.rotation);
				arrow.layer = gameObject.layer;
				var arrowScript = arrow.GetComponent<ProjectileTime>();
				arrowScript.CurrentSpeed = charger.Progress * ProjectileMaxSpeed;
				arrowScript.CurrentDirection = charger.Direction;
				arrowScript.LifeTimeLeft = ProjectileLifeTime;
				arrowScript.SlowdownPerSecond = ProjectileSlowdown;

				var force = -arrowScript.CurrentDirection * ProjectileRecoil * charger.Progress;
				rigid.AddForce(force, ForceMode2D.Impulse);
				SetEnergy(Energy - WeaponEnergyConsumtion * charger.Progress);
			}
			charging = false;
			charger.SetProgress(0);

		}


	}
	public void Pause(InputAction.CallbackContext action)
	{
		UIController.Elements["pause"].Show();
	}
	public void ChargingAttack(InputAction.CallbackContext action)
	{
		if(!charging)
		{
			charging = true;
			chargeStartPos = GetPointerPos();
		}

	}

	private void OnDoubleTap(MultiTapInput context)
	{
		if(context.TapCount >= 2 && DashCD.Progress == 0)
		{
			var force = gameObject.GetRelativeVector(GetPointerPos()).normalized * DashSpeed;
			rigid.AddForce(force, ForceMode2D.Impulse);
			DashCD.Show();
			DashCD.StartTimedProgression(DashCooldown, true);
		}
		
			
	}

	Vector2 GetPointerPos()
	{
		return Camera.main.ScreenToWorldPoint(Controls.Player.Pointer.ReadValue<Vector2>());
	}



	protected override void OnCollisionEnter2D(Collision2D collision)
	{
		base.OnCollisionEnter2D(collision);
		var hit = collision.GetContact(0);

		IBumpable bumpable = collision.gameObject.GetComponent<IBumpable>();
		if (bumpable != null && CanBump(bumpable))
			Bump(bumpable, gameObject.GetDirection(hit.point) * 2, hit.point);


	}

	private void OnDrawGizmosSelected()
	{
		
		if(charging)
		{
			Gizmos.color = Color.white;
			Vector2 dir = -transform.position.GetDirection(charger.transform.position);
			Gizmos.DrawRay(chargeStartPos + dir * NoChargeRadius, dir * MaxChargeRadius);

		}


	}
}
