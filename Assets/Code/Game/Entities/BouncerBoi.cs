﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BouncerBoi : BumpyMob
{
	public Vector2 ConstantVelocity;
	private Rigidbody2D rigid;
	protected override void Start()
	{
		base.Start();
		rigid = GetComponent<Rigidbody2D>();

	}

	protected override void FixedUpdate()
	{
		base.FixedUpdate();
		rigid.velocity = ConstantVelocity;

	}

	protected override void OnCollisionEnter2D(Collision2D collision)
	{
		base.OnCollisionEnter2D(collision);
		var hit = collision.GetContact(0);
		Debug.DrawRay(hit.point, hit.normal, Color.green, 0.3f); // draw green normal
		Debug.DrawRay(hit.point, -ConstantVelocity, Color.red, 0.3f); // draw red "in" velocity
		ConstantVelocity = Vector3.Reflect(ConstantVelocity, hit.normal); // reflect the velocity
		Debug.DrawRay(hit.point, ConstantVelocity, Color.blue, 0.3f); // draw blue "out" velocity

	}

}
