﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileDistance : MonoBehaviour, IMovable , IProjectileLifeDistance
{
	public bool IsMoving { get => RigidBody.velocity.magnitude >= 0 ? true : false; }
	public float CurrentSpeed { get; set; }
	public Vector3 CurrentDirection { get; set; }
	public float LifeDistanceLeft { get; set; } = 0;

	public float SlowdownPerSecond = 1;

	protected Rigidbody2D RigidBody;

	protected virtual void Awake()
	{
		RigidBody = GetComponent<Rigidbody2D>();
	}
	// Start is called before the first frame update
	void Start()
	{

	}
	public MovementError CanMove()
	{
		return MovementError.None;
	}

	public Vector3 Move(Vector3 direction, float maxSpeed)
	{
		RigidBody.velocity = direction.normalized * maxSpeed;
		return RigidBody.velocity * Time.deltaTime;
	}

	public void Stop()
	{
		RigidBody.velocity = Vector3.zero;
	}



    // Update is called once per frame
    void Update()
    {
        if(LifeDistanceLeft > 0)
		{
			LifeDistanceLeft -= Move(CurrentDirection, CurrentSpeed).magnitude;
			CurrentSpeed -= SlowdownPerSecond * Time.deltaTime;
			if(CurrentSpeed <= 0)
				Destroy(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
    }
}
