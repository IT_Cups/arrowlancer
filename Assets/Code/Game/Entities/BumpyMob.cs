﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BumpyMob : MonoBehaviour, IDamagable, IFaction, IBumpable, ITeleportable, IMob, IMovable, ICollider2D
{
	public float Health { get; set; }
	[SerializeField]
	[Range(0, 999999)]
	private float maxHealth = 1;
	public float MaxHealth { get => maxHealth; set => maxHealth = value; }
	public string FactionName => FactionData.Name;
	public TeleportGhost TeleportationGhost { get; set; }
	public bool IsBeingTeleported => TeleportationGhost ? true : false;

	public bool IsMoving => RigidBody.velocity.magnitude > 0 ? true : false;

	public Collider2D Collider { get; set; } = null;

	protected Rigidbody2D RigidBody;

	[SerializeField]
	private Faction factionData = new Faction();
	public Faction FactionData { get => factionData; }
	public float BumpForceMultiplier = 1;

	public GameObject SpawnOnDeathPrefab;


	protected virtual void Awake()
	{
		GameController.Mobs.Add(this);
		Health = MaxHealth;
		RigidBody = GetComponent<Rigidbody2D>();
		Collider = GetComponent<Collider2D>();
	}
	// Start is called before the first frame update
	protected virtual void Start()
	{
		
		
	}
	protected virtual void FixedUpdate()
	{

	}
	// Update is called once per frame
	protected virtual void Update()
	{

	}

	public virtual void TakeDamage(float amount)
	{
		Health -= amount;
		if (Health <= 0)
		{
			OnDeath();
			Destroy(gameObject);
		}
	}
	public virtual int GetStanding(IFaction faction)
	{
		return FactionData.GetStanding(faction.FactionName);
	}
	public virtual bool CanBump(IBumpable bumpedObject)
	{
		return true;
	}
	public virtual void Bump(IBumpable bumpedObject, Vector3 force, Vector3 point)
	{
		var otherFaction = bumpedObject as IFaction;
		var thisFaction = this as IFaction;
		if (otherFaction == null)
		{
			bumpedObject.gameObject.GetComponent<Rigidbody2D>().AddForceAtPosition(force, point, ForceMode2D.Impulse);
		}
		else
		{
			var standing = thisFaction.GetStanding(otherFaction);
			if (standing == 0)
			{
				bumpedObject.gameObject.GetComponent<Rigidbody2D>().AddForceAtPosition(force / 2, point, ForceMode2D.Impulse);
			}
			else if (standing < 0)
			{
				bumpedObject.gameObject.GetComponent<Rigidbody2D>().AddForceAtPosition(force, point, ForceMode2D.Impulse);
			}
		}
	}
	protected virtual void OnCollisionEnter2D(Collision2D collision)
	{
		var hit = collision.GetContact(0);

		IBumpable bumpable = collision.gameObject.GetComponent<IBumpable>();
		if (bumpable != null)
		{
			Rigidbody2D bumpedRB = bumpable.gameObject.GetComponent<Rigidbody2D>();
			if (CanBump(bumpable))
				Bump(bumpable, -bumpable.transform.WorldPositionToRelative(hit.point).normalized * Mathf.Max(BumpForceMultiplier, bumpedRB.velocity.magnitude), hit.point);
		}
		
		


	}
	public virtual bool CanBeTeleported()
	{
		if (!IsBeingTeleported)
			return true;
		return false;
	}
	public virtual void OnDeath()
	{

		var expOrb = Instantiate(SpawnOnDeathPrefab, transform.position, Quaternion.Euler(0f, 0f, Random.Range(0f, 360f)));
		expOrb.layer = LayerMask.NameToLayer("Player");
		var orbScript = expOrb.GetComponent<EnergyOrb>();
		orbScript.Energy = 5;
	}
	protected virtual void OnDestroy()
	{
		GameController.Mobs.Remove(this);
	}

	public MovementError CanMove()
	{
		return MovementError.None;
	}

	public Vector3 Move(Vector3 direction, float maxSpeed)
	{
		RigidBody.velocity = direction.normalized * maxSpeed;
		return RigidBody.velocity * Time.deltaTime;
	}

	public virtual void Stop()
	{
		RigidBody.velocity = Vector3.zero;
	}

	protected virtual void OnValidate()
	{
		FactionData.Validate();
	}
}
