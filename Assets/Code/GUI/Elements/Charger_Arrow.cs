﻿using MyBox.Internal;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Charger_Arrow : MonoBehaviour , IProgress, IUIElement
{
	public DirectionalCharger Charger;
	SpriteRenderer arrowSprite;
	public bool IsHidden { get { return !arrowSprite.enabled; } }
	public Vector3 ChargeVector => Charger.ChargeVector;

	private bool needUpdate = false;

	public Vector3 Direction
	{
		get => Charger.Direction;
		set
		{
			if (Charger.Direction != value)
			{
				Charger.Direction = value;
				needUpdate = true;
			}
		}
	}


	public string id => "ChargerArrow";

	public float Progress => Charger.Progress;

	[Range(0,1)]
	public float RevealSpriteThreshhold = 0.2f;
	public float RevealSpriteDuration = 1f;
	public float MinSize = 0.5f;

	public float MinArrowDistance = 0.3f;
	public float MaxArrowDistance = 1.5f;


	private Vector2 originalSize;

	public event Action OnComplete { add { Charger.OnComplete += value; } remove { Charger.OnComplete -= value; } }

	private void OnValidate()
	{
		Charger = new DirectionalCharger();
		transform.localPosition = Direction * MaxArrowDistance;
	}

    // Start is called before the first frame update
    void Awake()
    {
		Charger = new DirectionalCharger();
		arrowSprite = GetComponentInChildren<SpriteRenderer>();
		originalSize = arrowSprite.size;
	}

    // Update is called once per frame
    void Update()
    {
        if(needUpdate)
		{
			if (IsHidden && Progress >= RevealSpriteThreshhold)
			{
				Show();
				arrowSprite.UnfadeSprite(this, RevealSpriteDuration);
			}

			transform.localPosition = Mathf.Lerp(MinArrowDistance, MaxArrowDistance, Progress) * Direction;
			transform.rotation = Quaternion.LookRotation(Vector3.forward, Direction);

			arrowSprite.size = Progress * originalSize;

			needUpdate = false;
		}
    }

	public void Hide()
	{
		arrowSprite.enabled = false;
	}

	public void Show()
	{
		arrowSprite.enabled = true;
	}

	public void SetProgress(float value, bool triggerActions = true)
	{
		Charger.SetProgress(value, triggerActions);
		if (Progress == 0)
			Hide();
	}

	private void OnDrawGizmosSelected()
	{
		if(transform.parent)
		{
			Gizmos.color = Color.white;
			Gizmos.DrawRay(transform.parent.position, Direction * MaxArrowDistance);
		}

	}
}
