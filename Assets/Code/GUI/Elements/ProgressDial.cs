﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressDial : MonoBehaviour, IUIElement, IProgress
{
	public string id => "DashCD";
	private Canvas canvas;
	private Image image;

	public event Action OnComplete;

	public bool IsHidden => !canvas.enabled;
	public float Progress { get => image.fillAmount*100; set => SetProgress(value); }

	private Coroutine TimedProgression;

	public void Hide()
	{
		canvas.enabled = false;
	}

	public void Show()
	{
		canvas.enabled = true;
	}

	// Start is called before the first frame update
	void Awake()
    {
		canvas = GetComponent<Canvas>();
		Hide();
		canvas.worldCamera = Camera.main;
		image = GetComponent<Image>();
		UIController.AddElement(id, this);
	}

	public void SetProgress(float percent, bool triggerActions = true)
	{
		image.fillAmount = Mathf.Min(1, percent / 100);

		if(triggerActions && percent == 100)
		{
			OnComplete?.Invoke();
		}

	}

    // Update is called once per frame
    void Update()
    {
        
    }

	public void StartTimedProgression(float duration, bool reverse = false)
	{
		StopProgression();
		TimedProgression = StartCoroutine(ProgressTimer(duration, reverse));
	}

	public void StopProgression()
	{
		if(TimedProgression != null)
			StopCoroutine(TimedProgression);
	}

	private IEnumerator ProgressTimer(float duration, bool reverse = false)
	{
		float timeLeft = duration;
		do
		{
			yield return new WaitForEndOfFrame();
			timeLeft -= Time.deltaTime;
			var progress = timeLeft / duration * 100;
			if (!reverse)
				progress = 100 - progress;
			progress = Mathf.Clamp(progress, 0, 100);
			SetProgress(progress, false);
		}
		while (timeLeft > 0);
		OnComplete?.Invoke();
	}
}
