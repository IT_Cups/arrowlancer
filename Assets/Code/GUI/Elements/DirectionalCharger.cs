﻿using MyBox.Internal;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionalCharger : IProgress
{
	private float progress = 0;
	public float Progress { get => progress; set => SetProgress(value); }

	public Vector3 ChargeVector { get { return this.Direction * progress; } }

	private Vector3 direction = Vector3.up;

	public event Action OnComplete;

	public Vector3 Direction { get => direction; set => direction = value.normalized; }

	public void SetProgress(float percent, bool triggerActions = true)
	{
		progress = Mathf.Max(0, Mathf.Min(percent, 1));
		if (triggerActions && progress == 1)
			OnComplete?.Invoke();
	}
}
