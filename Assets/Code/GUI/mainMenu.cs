﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class mainMenu : MonoBehaviour
{
    public GameObject VictoryUI;
    public GameObject PauseUI;
    public void playGame()
    {
        if(!UIController.Elements.ContainsKey("victory"))
		{
			GameObject UIVictoryObject = Instantiate(VictoryUI);
			IUIElement elem = UIVictoryObject.GetComponent<IUIElement>();
			UIController.AddElement(elem.id, elem, true);
		}
		if (!UIController.Elements.ContainsKey("pause"))
		{
			GameObject UIPauseObject = Instantiate(PauseUI);
			IUIElement elem = UIPauseObject.GetComponent<IUIElement>();
			UIController.AddElement(elem.id, elem, true);
		}

        SceneManager.LoadScene(1);
    }
 

    public void quitGame()
    {
        Application.Quit();
    }
}
