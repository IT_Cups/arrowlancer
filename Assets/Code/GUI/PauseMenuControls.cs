﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuControls : MonoBehaviour, IUIElement
{
    private Canvas canvas;

    public bool IsHidden => !canvas.enabled;

    public string id => "pause";
    // Start is called before the first frame update
    void Start()
    {
        canvas = GetComponent<Canvas>();
        Hide();
        canvas.worldCamera = Camera.main;
    }

    public void Show()
    {
        canvas.enabled = true;
        Pause();
    }
    public void Hide()
    {
        canvas.enabled = false;
        UnPause();
    }
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Hide();
    }
    public void BackToMenu()
    {
        SceneManager.LoadScene(0);
        Hide();
    }

    public void Pause()
    {
        Time.timeScale = 0;
    }

    public void UnPause()
    {
        Time.timeScale = 1;
    }
}
