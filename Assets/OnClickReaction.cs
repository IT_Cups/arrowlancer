﻿using UnityEngine.InputSystem.PointerManager;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnClickReaction : MonoBehaviour
{
	public bool Active = true;
	public UnityEvent<Vector2> OnComplete;
	private PointerInputManager Controls;
	private Collider2D col;
	

	// Start is called before the first frame update
	void Start()
    {
		col = GetComponent<Collider2D>();
		Controls = GameController.Players[GameController.PlayerNumber.Player1].PointerManager;
		if (!Controls)
		{
			Debug.LogError("Couldn't get a PointerManager.");
		}
		Controls.Pressed += OnPressed;

	}

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnPressed(PointerInput input, double time)
	{
		Collider2D hitCollider = Physics2D.OverlapPoint(Camera.main.ScreenToWorldPoint(input.Position));
		if (hitCollider == col && Active)
		{
			OnComplete?.Invoke(input.Position);
		}
		
	}
}
