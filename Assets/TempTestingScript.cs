﻿/*
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempTestingScript : MonoBehaviour
{
	// Start is called before the first frame update
	MeshRenderer rend;
	Mesh mesh;

	public float fov = 90f;
	[SerializeField] private int rayCount = 2;
	public float initialAngle = 0;
	public float viewDistance = 6f;
	[SerializeField]
	private LayerMask layerMask;
	private Vector3 origin = Vector3.zero;
	private int oldRayCount;

	void Awake()
    {
		rend = GetComponent<MeshRenderer>();
		mesh = new Mesh();
		mesh.MarkDynamic();
		GetComponent<MeshFilter>().mesh = mesh;
		GetComponent<MeshCollider>().sharedMesh = mesh;
		UpdateMesh();
	}

    // Update is called once per frame
    void Update()
    {
		//SetOrigin(transform.position);
		//SetAimDirection(transform.eulerAngles);
		UpdateMesh();
		//mesh.RecalculateBounds();
	}

	private void OnValidate()
	{
		if(mesh == null)
		{
			mesh = new Mesh();
			mesh.MarkDynamic();
			GetComponent<MeshFilter>().mesh = mesh;
			GetComponent<MeshCollider>().sharedMesh = mesh;

		}
		//SetOrigin(transform.position);
		//SetAimDirection(transform.eulerAngles);
		UpdateMesh();

	}

	void UpdateMesh()
	{

		float angle = initialAngle;
		float angleIncrease = fov / rayCount;

		Vector3[] verticies = new Vector3[rayCount+1+1];
		Vector2[] uv = new Vector2[verticies.Length];
		int[] triangles = new int[rayCount*3];

		verticies[0] = origin;
		int vertexIndex = 1;
		int triangleIndex = 0;

		for (int i=0; i<=rayCount; i++)
		{
			Vector3 vertex;
			
			RaycastHit2D hit = Physics2D.Raycast(transform.RelativePositionToWorld(origin), EngineHelper.GetVectorFromAngle(angle), viewDistance, layerMask);
			if(hit.collider == null)
			{
				vertex = origin + EngineHelper.GetVectorFromAngle(angle) * viewDistance;
			}
			else
			{
				vertex = hit.point - (Vector2)transform.RelativePositionToWorld(origin);
			}
			verticies[vertexIndex] = vertex;
			if (i > 0)
			{
				triangles[triangleIndex] = 0;
				triangles[triangleIndex + 1] = vertexIndex - 1;
				triangles[triangleIndex + 2] = vertexIndex;
				triangleIndex += 3;
			}
			vertexIndex++;
			angle -= angleIncrease;
		}

		mesh.vertices = verticies;
		mesh.uv = uv;

		mesh.triangles = triangles;
		
		
	}

	public void SetOrigin(Vector3 origin)
	{
		this.origin = origin;
	}
	public void SetAimDirection(Vector3 aimDirection)
	{
		initialAngle = EngineHelper.GetAnglerFromVector(aimDirection) - fov/2f;
	}
	protected virtual void OnCollisionEnter2D(Collision2D collision)
	{
		Debug.Log("Collidedede");

	}

}
*/